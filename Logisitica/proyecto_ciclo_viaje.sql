Use gstdb;

--Query/vista para determinar los campos del reporte ciclo de viaje
use gstdb;
ALTER VIEW gst_ciclo_viaje
as
SELECT 
 TG.id_area,TG.num_guia as 'CartaPorte'
,DP.num_pedido as 'NumPedido'
,TG.no_viaje as 'NoViaje'
,TG.personalnombre as 'Operador'
,TC1.nombre as 'ClientePaga'
,TC2.nombre as 'Remitente'
,TC3.nombre as 'Destinatario'
,TG.id_unidad as 'Unidad'
,TG.fecha_guia as 'Fecha'
,TP1.desc_plaza as 'Origen'
,TP2.desc_plaza as 'Destino'
,TG.kms_guia as 'kms'
--,'TiempoCarga','TiempoDescarga','TiempoTraslado','TiempoCiclo'
,DP.f_carini_prog as 'Carga_ini_programdo'
,DP.f_carini_real as 'Carga_ini_real'
,DP.f_carfin_prog as 'Carga_fin_programdo'
,DP.f_carfin_real as 'Carga_fin_real'
,DP.f_arribo_prog as 'Cita_cliente_programado' 
,DP.f_arribo_real as 'Cita_cliente_real'
,DP.f_desini_prog as 'Descarga_ini_programado'
,DP.f_desfin_real as 'Descarga_ini_real'
,DP.f_desfin_prog as 'Descarga_fin_programado'
,DP.f_desfin_real as  'Descarga_fin_real'
from gstdb.dbo.trafico_guia TG
INNER JOIN gstdb.dbo.desp_pedido DP on TG.id_area = DP.id_area and TG.no_guia = DP.no_guia
INNER JOIN gstdb.dbo.trafico_cliente TC1 on TG.id_cliente = TC1.id_cliente 
INNER JOIN gstdb.dbo.trafico_cliente TC2 on TG.id_remitente = TC2.id_cliente 
INNER JOIN gstdb.dbo.trafico_cliente TC3 on TG.id_destinatario = TC3.id_cliente 
INNER JOIN gstdb.dbo.trafico_plaza TP1 on TG.id_origen = TP1.id_plaza 
INNER JOIN gstdb.dbo.trafico_plaza TP2 on TG.id_destino = TP2.id_plaza 
WHERE TG.id_area = 8 and TG.fecha_guia >='09-01-2019' and TG.id_cliente <> 1 and TG.no_viaje <> '0' and TG.num_guia <>'B'
--and TG.num_guia ='CU-175328'

--Query/vista para determinar los tiempos de cada estatus de viaje
--use gstdb;
--CREATE VIEW gst_bitacora_status_viaje
--as
SELECT
	 DBV.id_area
	,DBV.no_viaje as 'NoViaje'
	,DBV.id_statusviaje as '#Evento'
	,DSV.nombre_status_viaje as 'Estatusviaje'
	,DBV.f_ini_status
	,DBV.f_fin_status
	,TV.f_despachado,TV.fecha_real_viaje,TV.fecha_real_fin_viaje
FROM
	gstdb.dbo.desp_statusviaje DSV
INNER JOIN gstdb.dbo.desp_bitacorastatusviaje DBV on
	DSV.id_statusviaje = DBV.id_statusviaje
INNER JOIN 
	gstdb.dbo.trafico_viaje TV on DBV.id_area = TV.id_area and DBV.no_viaje = TV.no_viaje
ORDER BY
	DBV.id_area,
	DBV.no_viaje

--Vista General Uniendo los datos de la cp/pedido con la informacion del viaje	
SELECT
	*
FROM
	gstdb.dbo.gst_ciclo_viaje CC
INNER JOIN gstdb.dbo.gst_bitacora_status_viaje BSV on
	CC.id_area = BSV.id_area
	and CC.noviaje = BSV.noviaje
	

	
select * from gstdb.dbo.gst_ciclo_viaje