 --FDSC					Fecha:17/09/2019   
 --Folio: 27050			Ticket: #025832
 --Descripci�n: Modificaci�n a la implementacion de la query para la columna Cargado/Vac�o en las tablas en las que esta presente para mostrar como Cargados a los viajes 
 --que estuvieron involucrados en un reembarque esto en la pantalla de Consulta-Viajes 

IF NOT EXISTS(SELECT * FROM general_parametros WHERE nombre = 'cargadoVacioReembarques')
	INSERT INTO general_parametros
	(nombre , valor, modulo, descripcion, tipo, id_parametro)
	VALUES('cargadoVacioReembarques', 'S', 'OPERACIONES' , 'Mostrar los viajes que realizaron un reembarque como cargados' , 'S' , NULL )
GO	

IF NOT EXISTS(SELECT * from ventana_columnas WHERE id_ventana = 5 AND consecutivo = 26 AND nombre ='Cargado/Vac�o')
	INSERT INTO ventana_columnas
	(id_ventana, consecutivo , nombre , nombre_interno, tipo_dato , formato , ancho_columna, alinear , alias, requerido, visible, editar)
	VALUES(5 , 26 , 'Cargado/Vac�o','(SELECT CASE WHEN ( ( Isnull(Max(dad.id_pedido), 0) > 0 ) OR EXISTS(SELECT * FROM   trafico_reembarcar WHERE ((trafico_viaje.no_viaje =  trafico_reembarcar.viaje1 OR trafico_viaje.no_viaje =  trafico_reembarcar.viaje2) AND trafico_viaje.id_area = trafico_reembarcar.id_area)))THEN ''Cargado'' ELSE ''Vacio'' END FROM   desp_asignaciondet dad WHERE  dad.id_asignacion = desp_asignacion.id_asignacion)', 'texto', 'string', 100, 'center','cargado_vacio' , 1, 1 , 0  )
	GO
	 
IF EXISTS(SELECT * from ventana_columnas WHERE id_ventana = 5 AND consecutivo = 26 AND nombre = 'Cargado/Vac�o')
	UPDATE ventana_columnas
	SET nombre_interno = '(SELECT CASE WHEN ( ( Isnull(Max(dad.id_pedido), 0) > 0 ) OR EXISTS(SELECT * FROM   trafico_reembarcar WHERE ((trafico_viaje.no_viaje =  trafico_reembarcar.viaje1 OR trafico_viaje.no_viaje =  trafico_reembarcar.viaje2) AND trafico_viaje.id_area = trafico_reembarcar.id_area)))THEN ''Cargado'' ELSE ''Vacio'' END FROM   desp_asignaciondet dad WHERE  dad.id_asignacion = desp_asignacion.id_asignacion)'
WHERE id_ventana = 5 AND consecutivo = 26 AND nombre = 'Cargado/Vac�o'
GO
		
IF EXISTS(SELECT * from vista WHERE id_ventana = 5)
	UPDATE vista_query
	SET dselect =  REPLACE(CAST(dselect as varchar(8000))
	,'(select case when isnull(max(dad.id_pedido),0) > 0 then ''Cargado'' else ''Vacio'' end from desp_asignaciondet dad where dad.id_asignacion = desp_asignacion.id_asignacion )'
	,'(SELECT CASE WHEN ( ( Isnull(Max(dad.id_pedido), 0) > 0 ) OR EXISTS(SELECT * FROM   trafico_reembarcar WHERE ((trafico_viaje.no_viaje =  trafico_reembarcar.viaje1 OR trafico_viaje.no_viaje =  trafico_reembarcar.viaje2) AND trafico_viaje.id_area = trafico_reembarcar.id_area)))THEN ''Cargado'' ELSE ''Vacio'' END FROM   desp_asignaciondet dad WHERE  dad.id_asignacion = desp_asignacion.id_asignacion)' )
	WHERE id_vista IN(SELECT id_vista from vista  WHERE id_ventana = 5)
GO

IF EXISTS(SELECT * from vista WHERE id_ventana = 5)
	UPDATE vista_consultadet
	SET nombre_columna = '(SELECT CASE WHEN ( ( Isnull(Max(dad.id_pedido), 0) > 0 ) OR EXISTS(SELECT * FROM   trafico_reembarcar WHERE ((trafico_viaje.no_viaje =  trafico_reembarcar.viaje1 OR trafico_viaje.no_viaje =  trafico_reembarcar.viaje2) AND trafico_viaje.id_area = trafico_reembarcar.id_area)))THEN ''Cargado'' ELSE ''Vacio'' END FROM   desp_asignaciondet dad WHERE  dad.id_asignacion = desp_asignacion.id_asignacion)'
	WHERE nombre = 'Cargado/Vac�o' 
GO