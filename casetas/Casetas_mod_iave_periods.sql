
-- how many years count after 2005 

select * from sistemas.dbo.casetas_view_build_iave_ids

use sistemas
IF OBJECT_ID ('casetas_view_build_iave_ids', 'V') IS NOT NULL		
    DROP VIEW casetas_view_build_iave_ids;
create view casetas_view_build_iave_ids
with encryption
as		
with "years" as	 (
					select 
							 1 as "num"
						union all
				    select 
							 "num" + 1 as "num"
					from 
				    		"years"
				    where
							"num" <= (DATEDIFF ( year , '2005-01-01' , CURRENT_TIMESTAMP ))
) -- select * from "years"
select
		row_number()
		over
			(order by cyear) as
				"period_iave_id"
				,"cyear"
				,"id_month"
				,"month"
				,"date"
				,"period"
				,"per_id"
				,"init"
				,"end"
				,"period_desc"
				,"fecha_ini"
				,"fecha_fin"
				,"_control"
	from
		(
			select
					 year(dateadd(year,"yr"."num",'2005-01-01')) as "cyear"
					,"mts".id_month
					,"mts"."month"
					,cast(year(dateadd(year,"yr"."num",'2005-01-01')) as varchar(4)) + '-' + cast("mts".id_month as varchar(2)) + '-01' as "date"
					,cast(year(dateadd(year,"yr"."num",'2005-01-01')) as varchar(4)) + "mts".id_month as "period"
					,"per".per_id
					,"per".init as "init"
					,cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num",'2005-01-01'))) - 1900) * 12) + "mts".id_month, -1) ))) as char) as "end"
					,"mts"."month" + ' ' + "per".init + ' al ' + cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num",'2005-01-01'))) - 1900) * 12) + "mts".id_month, -1) ))) as char) as "period_desc"
					,cast(year(dateadd(year,"yr"."num",'2005-01-01')) as varchar(4)) + '-' + "mts".id_month + '-' + right('00'+convert(varchar(2),"per".init), 2) +' 00:00:00.000' as "fecha_ini"
					,cast(year(dateadd(year,"yr"."num",'2005-01-01')) as varchar(4)) + '-' + "mts".id_month + '-' + cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num",'2005-01-01'))) - 1900) * 12) + "mts".id_month, -1) ))) as varchar) +' 23:59:00.000' as "fecha_fin"
					,'1' as '_control'
			from
					"years" as "yr"
				left join (
							select '01' as "id_month", 'Enero' as "month"
							union select '02' as "id_month", 'Febrero' as "month"
							union select '03' as "id_month", 'Marzo' as "month"
							union select '04' as "id_month", 'Abril' as "month"
							union select '05' as "id_month", 'Mayo' as "month"
							union select '06' as "id_month", 'Junio' as "month"
							union select '07' as "id_month", 'Julio' as "month"
							union select '08' as "id_month", 'Agosto' as "month"
							union select '09' as "id_month", 'Septiembre' as "month"
							union select '10' as "id_month", 'Octubre' as "month"
							union select '11' as "id_month", 'Noviembre' as "month"
							union select '12' as "id_month", 'Diciembre' as "month"
						   )
				as "mts" on 1 = 1
				left join (
							select '1' as 'per_id', '1' as 'init', null as 'end'
--							select '1' as 'per_id', '1' as 'init', '10' as 'end'
--							union select '2' as 'per_id', '11' as 'init', '20' as 'end'
--							union select '3' as 'per_id', '21' as 'init', null as 'end'
						   )
				as "per" on 1 = 1				
		)
	as result
	
	
	
	
--create a job to fill next table ;
	insert into sistemas.dbo.casetas_periods_iave_ids

		select 
			period_iave_id,cyear,id_month,"month","date",period,per_id,init,"end",period_desc,fecha_ini,fecha_fin,_control
			,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,null,1
		from 
			sistemas.dbo.casetas_view_build_iave_ids
	

select * from sistemas.dbo.casetas_view_resume_stands	


	
select * from sistemas.dbo.casetas_iave_new_periods_sources 

use sistemas
IF OBJECT_ID ('casetas_iave_new_periods_sources', 'V') IS NOT NULL		
    DROP VIEW casetas_iave_new_periods_sources;
create view casetas_iave_new_periods_sources
with encryption
as		
	select 
			row_number() over(order by "period".period) as 'period_iave_id'
--			,"period".period_iave_id
			,"period".cyear
			,"period".id_month
			,"period"."month"
			,"period"."date"
			,"period".period
			,"period".per_id
			,"period".init,"period"."end","period".period_desc,"period".fecha_ini,"period".fecha_fin,"period"."_control"
	from 
			sistemas.dbo.casetas_view_build_iave_ids as "period"
	where 
			period > 200610
	
	-- Not in Use
	select * from sistemas.dbo.casetas_periods_iave_ids
	
--	do not use until
	use sistemas
	IF OBJECT_ID('casetas_periods_iave_ids', 'U') IS NOT NULL 
	  DROP TABLE casetas_periods_iave_ids; 
	
	set ansi_nulls on
	set quoted_identifier on
	set ansi_padding on
	
	create table [casetas_periods_iave_ids](
			 id						int identity(1,1)
			,period_iave_id			int 
			,cyear					int
			,id_month				char(2)
			,"month"				char(12)
			,"date"					date
			,period					int
			,per_id					int
			,init					int
			,"end"					int
			,period_desc			char(25)
			,fecha_ini				datetime
			,fecha_fin				datetime
			,"_control"				tinyint default 1
			,created				datetime
			,modified				datetime
			,user_id				int null
			,"status"				tinyint default 1 null
	) on [primary]
	
	set ansi_padding off
	
--	=========================================================================================================================	
	
	
	
	insert into  
		(CpnyId,BatNbr,RefNbr,SATProductId,SATUnitId,LineNbr,S4Future01,S4Future02,S4Future03,S4Future04,S4Future05,S4Future06,S4Future07,S4Future08,S4Future09,S4Future10,S4Future11,S4Future12,User1,User2,User3,User4,User5,User6,User7,User8,XPredial)
		--,tstamp
	values (xmamamakmdamkdakmlda,'','',)
	
	
	from integraapp.dbo.XGRWARTrans
	
	
	
	
-- =================================================== re-build ========================================================== --	
--	add 1 to periods in starting period 
	
with "years" as	 (
					select 
							 1 as "num"
							 ,-2 as 'monthconf'
							 ,cast(dateadd(year,1,'2006-01-01') as date) as "inity"
						union all
				    select 
							 "num" + 1 as "num"
 							 ,-2 as 'monthconf'
 							 ,cast(dateadd(year,"num" + 1,'2006-01-01') as date) as "inity"
					from 
				    		"years"
				    where
							"num" <= (DATEDIFF ( year , '2006-01-01' , CURRENT_TIMESTAMP ))
) --
select * from "years"

select
		row_number()
		over
			(order by cyear) as
				"period_iave_id"
				,"cyear"
				,"id_month"
				,"month"
				,"ndate"
				,"date"
				,"period"
				,"per_id"
				,"init"
				,"end"
				,"period_desc"
				,"fecha_ini"
				,"fecha_fin"
				,"_control"
	from
		(
			select
					 "yr"."inity" as "cyear"
					,"mts".id_month
					,"mts"."month"
					,cast(year("yr"."inity") as varchar) + '-' + cast("mts".id_month as varchar) + '-01' as "date"
					,dateadd(month,"yr"."monthconf",cast(year("yr"."inity") as varchar) + '-' + cast("mts".id_month as varchar) + '-01') as "ndate"
--					,cast(year(dateadd(year,"yr"."num",'2005-01-01')) as varchar(4)) + '-' + cast("mts".id_month as varchar(2)) + '-01' as "ndate"
--					,cast(year(dateadd(year,"yr"."num","yr"."inity")) as varchar(4)) + "mts".id_month as "period"
					,substring( convert(nvarchar(MAX), dateadd(month,"yr"."monthconf",cast(year("yr"."inity") as varchar) + '-' + cast("mts".id_month as varchar) + '-01'), 112) , 1, 6 ) as "period"
					,"per".per_id
					,"per".init as "init"
					,cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num","yr"."inity"))) - 1900) * 12) + "mts".id_month, -1) ))) as char) as "end"
					,"mts"."month" + ' ' + "per".init + ' al ' + cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num","yr"."inity"))) - 1900) * 12) + "mts".id_month, -1) ))) as char) as "period_desc"
					, cast(year(dateadd(year,"yr"."num","yr"."inity")) as varchar(4)) + '-' + "mts".id_month + '-' + right('00'+convert(varchar(2),"per".init), 2) +' 00:00:00.000' as "fecha_ini"
					, cast(year(dateadd(year,"yr"."num","yr"."inity")) as varchar(4)) + '-' + "mts".id_month + '-' + cast(isnull("per"."end",day(( DATEADD(month, (((year(dateadd(year,"yr"."num","yr"."inity"))) - 1900) * 12) + "mts".id_month, -1) ))) as varchar) +' 23:59:00.000' as "fecha_fin"
					,'1' as '_control'
			from
					"years" as "yr"
				left join (
							select '01' as "id_month", 'Enero' as "month"
							union select '02' as "id_month", 'Febrero' as "month"
							union select '03' as "id_month", 'Marzo' as "month"
							union select '04' as "id_month", 'Abril' as "month"
							union select '05' as "id_month", 'Mayo' as "month"
							union select '06' as "id_month", 'Junio' as "month"
							union select '07' as "id_month", 'Julio' as "month"
							union select '08' as "id_month", 'Agosto' as "month"
							union select '09' as "id_month", 'Septiembre' as "month"
							union select '10' as "id_month", 'Octubre' as "month"
							union select '11' as "id_month", 'Noviembre' as "month"
							union select '12' as "id_month", 'Diciembre' as "month"
						   )
				as "mts" on 1 = 1
				left join (
							select '1' as 'per_id', '1' as 'init', null as 'end'
--							select '1' as 'per_id', '1' as 'init', '10' as 'end'
--							union select '2' as 'per_id', '11' as 'init', '20' as 'end'
--							union select '3' as 'per_id', '21' as 'init', null as 'end'
						   )
				as "per" on 1 = 1				
		)
	as result
	
	
	
exec sp_who2


dbcc inputbuffer(1005 )

dbcc opentran()

use master

-- === Search if a table has an index

SELECT
    sys.tables.name,
    sys.indexes.name,
    sys.columns.name
FROM sys.indexes
    INNER JOIN sys.tables ON sys.tables.object_id = sys.indexes.object_id
    INNER JOIN sys.index_columns ON sys.index_columns.index_id = sys.indexes.index_id
        AND sys.index_columns.object_id = sys.tables.object_id
    INNER JOIN sys.columns ON sys.columns.column_id = sys.index_columns.column_id
        AND sys.columns.object_id = sys.tables.object_id
--WHERE sys.tables.name = 'GLTran'
ORDER BY
    sys.tables.name,
    sys.indexes.name,
    sys.columns.name
    
    
  use integraapp
    exec sp_helpindex GLTran
    
-- ===========   are dormant process ?? 

use integraapp
SELECT
    [s_tst].[session_id],
    [s_es].[login_name] AS [Login Name],
    DB_NAME (s_tdt.database_id) AS [Database],
    [s_tdt].[database_transaction_begin_time] AS [Begin Time],
    [s_tdt].[database_transaction_log_bytes_used] AS [Log Bytes],
    [s_tdt].[database_transaction_log_bytes_reserved] AS [Log Rsvd],
    [s_est]."text" AS [Last T-SQL Text],
    [s_eqp].[query_plan] AS [Last Plan]
FROM
    sys.dm_tran_database_transactions [s_tdt]
JOIN
    sys.dm_tran_session_transactions [s_tst]
ON
    [s_tst].[transaction_id] = [s_tdt].[transaction_id]
JOIN
    sys.[dm_exec_sessions] [s_es]
ON
    [s_es].[session_id] = [s_tst].[session_id]
JOIN
    sys.dm_exec_connections [s_ec]
ON
    [s_ec].[session_id] = [s_tst].[session_id]
LEFT OUTER JOIN
    sys.dm_exec_requests [s_er]
ON
    [s_er].[session_id] = [s_tst].[session_id]
CROSS APPLY
    sys.dm_exec_sql_text ([s_ec].[most_recent_sql_handle]) AS [s_est]
OUTER APPLY
    sys.dm_exec_query_plan ([s_er].[plan_handle]) AS [s_eqp]
ORDER BY
    [Begin Time] ASC;

    
    
   
-----------------

    SELECT 
     es.session_id AS session_id

    ,CASE WHEN eS.LOGIN_NAME = eS.ORIGINAL_LOGIN_NAME 
     THEN eS.LOGIN_NAME 
     ELSE eS.LOGIN_NAME + ' (' + eS.ORIGINAL_LOGIN_NAME + ')' 
     END AS LOGIN_NAME

    ,es.host_name AS hostname
    ,es.status
    ,es.last_request_start_time
    ,es.last_request_end_time
    ,es.CPU_TIME AS CPU_TIME_MS
    ,es.MEMORY_USAGE AS MEMORY_USAGE_PAGES
    ,es.ROW_COUNT
    ,NULL AS blocked_by
    ,NULL AS waittype
    ,NULL AS waittime
    ,NULL AS lastwaittype
    ,NULL AS waitresource
    ,dbid=trans.database_name
    ,NULL as  cmd
    ,t.text AS [QUERY]
    ,NULL AS PARENT_QUERY

   ,TRANSACTION_ISOLATION_LEVEL = NULL

    ,NULL as cpu

    ,NULL AS physical_io

    --,es.open_transaction_count as [open_tran]
    ,es.program_name

    --,es.login_time
    --,ota.task_state
    --,ota.pending_io_count
    --,ota.pending_io_byte_count


    FROM sys.dm_exec_sessions AS es
    INNER JOIN sys.dm_exec_connections AS c
        ON es.session_id = c.session_id

    --LEFT OUTER JOIN sys.dm_os_tasks ota ON es.session_id = ota.session_id

    CROSS APPLY (

        SELECT MAX(DB_NAME(dt.database_id)) AS database_name
        FROM sys.dm_tran_session_transactions AS st
        INNER JOIN sys.dm_tran_database_transactions AS dt
            ON st.transaction_id = dt.transaction_id
        WHERE is_user_transaction = 1
        GROUP BY st.session_id
        HAVING es.session_id = st.session_id 

    ) AS trans

    CROSS APPLY sys.dm_exec_sql_text(c.most_recent_sql_handle) AS t
    WHERE es.session_id NOT IN (

            SELECT session_id
            FROM sys.dm_exec_requests

        )
        AND es.session_id IN (

            SELECT request_session_id
            FROM sys.dm_tran_locks
            WHERE request_status = 'GRANT'
        )
        AND STATUS = 'sleeping'
        AND is_user_process = 1
        AND es.session_id <> @@spid

------------

SELECT 
     s.session_id
    ,s.status
    ,s.login_time
    ,s.host_name
    ,s.program_name
    ,s.host_process_id
    ,s.original_login_name
    ,s.last_request_end_time
    ,CAST(t.text AS nvarchar(4000)) AS [text]
FROM sys.dm_exec_sessions AS s
INNER JOIN sys.dm_exec_connections AS c
    ON s.session_id = c.session_id
CROSS APPLY (
    SELECT MAX(DB_NAME(dt.database_id)) AS database_name
    FROM sys.dm_tran_session_transactions AS st
    INNER JOIN sys.dm_tran_database_transactions AS dt
        ON st.transaction_id = dt.transaction_id
    WHERE is_user_transaction = 1
    GROUP BY st.session_id
    HAVING s.session_id = st.session_id 
) AS trans
CROSS APPLY sys.dm_exec_sql_text(most_recent_sql_handle) AS t
WHERE s.session_id NOT IN (
        SELECT session_id
        FROM sys.dm_exec_requests
    )
    AND s.session_id IN (
        SELECT request_session_id
        FROM sys.dm_tran_locks
        WHERE request_status = 'GRANT'
    )
    AND STATUS = 'sleeping'
    AND is_user_process = 1

	