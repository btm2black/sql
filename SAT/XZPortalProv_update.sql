USE [integraapp]
GO
/****** Object:  StoredProcedure [dbo].[XZPortalProv_Update_ARDoc]    Script Date: 11/11/2019 16:50:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[XZPortalProv_Update_ARDoc] As

Begin Tran UpdateARDoc

Update ZPortalProv
  Set ZEstatus = 'Z'
  Where ZEstatus = '1' And
        ZBatNbr  <> ''

If @@ERROR <> 0
  Goto Error

Update a
  Set a.DueDate	 = dbo.XGetDueDate(z.ZInvcDate, d.ZDiaPago, t.DueIntrv),
      a.InvcDate = z.ZInvcDate,
      a.InvcNbr	 = z.ZInvcNbr,
      a.PayDate	 = dbo.XGetDueDate(z.ZInvcDate, d.ZDiaPago, t.DueIntrv),
      a.PerPost  = z.ZPerPost,
      a.User1    = 'EC'
  From APDoc a Inner Join ZPortalProv z (Nolock) On a.RefNbr    = z.ZAPRefno And
                                                    a.CpnyId    = z.ZCpnyId	And
                                                    a.VendId    = z.ZVendId And
                                                    z.ZEstatus  = 'Z'
       Inner Join Terms t (Nolock) On a.Terms = t.TermsId
       Inner Join ZDiasPago (Nolock) d On a.CpnyId = d.ZCpnyId
  Where a.DocType   = 'VO' And
        a.Rlsed     = 0

If @@ERROR <> 0
  Goto Error

Insert Into WrkRelease 
  Select a.BatNbr,
         'AP',
         'SQLEC',
         Null
  From APDoc a Inner Join ZPortalProv z (Nolock) On a.RefNbr    = z.ZAPRefno And
                                                    a.CpnyId    = z.ZCpnyId	And
                                                    a.VendId    = z.ZVendId And
                                                    z.ZEstatus  = 'Z'
  Where a.DocType   = 'VO' And
        a.Rlsed     = 0

If @@ERROR <> 0
  Goto Error

-- Llamar el SP de liberación de CxP
Exec pp_03400 'SQLEC', 'SQLEC'    

Delete w
  From  WrkRelease w 
  Inner Join APDoc a On a.BatNbr = w.BatNbr
  Inner Join ZPortalProv z (Nolock) On a.RefNbr    = z.ZAPRefno And
                                       a.CpnyId    = z.ZCpnyId	And
                                       a.VendId    = z.ZVendId And
                                       Convert(dec(28, 2), a.TxblTot00) = Convert(dec(28, 2), z.ZCuryRcptCtrlAmt) And
                                       z.ZEstatus  = 'Z'
  Where a.DocType   = 'VO'

Delete w
  From  WrkReleaseBad w
  Inner Join APDoc a On a.BatNbr = w.BatNbr
  Inner Join ZPortalProv z (Nolock) On a.RefNbr    = z.ZAPRefno And
                                       a.CpnyId    = z.ZCpnyId	And
                                       a.VendId    = z.ZVendId And
                                       Convert(dec(28, 2), a.TxblTot00) = Convert(dec(28, 2), z.ZCuryRcptCtrlAmt) And
                                       z.ZEstatus  = 'Z'
  Where a.DocType   = 'VO'
    
Insert Into WrkRelease 
  Select ZBatNbr,
         'AP',
         'SQLEC',
         Null
    From ZPortalProv
    Where ZEstatus = 'Z'
    
Update z
  Set z.ZAcct    = a.Acct,
      z.ZPayDate = a.PayDate,
      z.ZEstatus = '2'
  From ZPortalProv z Inner Join APDoc a (Nolock) On z.ZAPRefno = a.RefNbr And
                                                    z.ZCpnyId  = a.CpnyId And
                                                    z.ZVendId  = a.VendId
  Where z.ZEstatus = 'Z'

If @@ERROR <> 0
  Goto Error

Commit Tran LoadARInvoices

Goto Finished

Error:
  RollBack Tran UpdateARDoc
  
Finished:
