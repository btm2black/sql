
with "lois" as (
		
--use sistemas
--select top 100 * from sistemas.dbo.holcim_conciliation_toneladas 
---- select * from sistemas.dbo.ws_georeference_view_positions_gst_indicators where base like '%modelo%'   
--IF OBJECT_ID ('holcim_conciliation_toneladas', 'V') IS NOT NULL
--    DROP VIEW holcim_conciliation_toneladas;
---- now build the view
--create view holcim_conciliation_toneladas
--with encryption
--as	

		select 
				"lois".area as 'lis-area'
				,"lois".tipo_de_operacion as 'lis-operacopn'
				,"lois".f_despachado_m as 'lis-f_despachado'
				,"lois".no_remision	as 'lis-remision'
				,"olcim".remision
				,"lois".[peso-despachado] as [lis-peso-despachado]
				,"olcim".tons
				,case 
					when "lois".status_guia <> 'B'
					then 'ACEPTADO'
					else 'CANCELADA'
				 end as 'EstatusGuia'
				,case 
					when 
						"lois".no_remision is null or "lois".no_remision = '' and "olcim".remision is not null
					then
						'sin remision en lois'
					when 
						"olcim".remision is null or "olcim".remision = '' and "lois".no_remision is not null
					then 
						'sin remision en olcim'
					else 
						'Conciliado'
				end as 'validation'
				,case 
					when 
						"lois".no_remision is null or "lois".no_remision = '' and "olcim".remision is not null
					then
						1
					else
						0
				end as 'lois'
				,case 			
					when 
						"olcim".remision is null or "olcim".remision = '' and "lois".no_remision is not null
					then 
						1
					else 
						0
				end as 'olcim'
						
		--		,sum("lois".subtotal) as 'subtotal' 
		from 
				sistemas.dbo.projections_view_full_gst_testcore_indicators as "lois"
		full join 
				sistemas.dbo.gst_holcim as "olcim" 
			on 
				ltrim(rtrim("lois".no_remision)) = ltrim(rtrim("olcim".remision))
		where 
--				(
				"lois".FlagIsDisminution <> 2
-- and 
		-- 	 	"lois".flagIsProvision = 1 --Prestamo
		and 
				"lois".projections_rp_definition = 'GRANEL'
		and 
			 	"lois".FlagIsNextMonth = 0 --negativo
		and 
				"lois".f_despachado_m between 201901 and 201906
--			)
		or
				"lois".no_remision is null and "lois".area is null
		--and 
		--		"lois".area = 'ORIZABA'
--		and 
--				"olcim".remision = '326042474'
)
select * from "lois" where  [lis-remision] is null -- remision = 326042474 --


		sum([lis-peso-despachado]) as 'lis'
		,sum(lois) as 'sin_conciliar_lis'
		,sum(tons) as 'olcim' 
		,sum(olcim) as 'sin_conciliar_olcim'
		,[lis-area]
		,[lis-f_despachado] from "lois"
group by
		 [lis-f_despachado]
		,[lis-area]
	
	
	
	


select * from sistemas.dbo.gst_holcim

select no_remision,no_viaje,* from gstdb.dbo.trafico_guia
where no_remision in ('23529969','324961507')

select f_despachado,* from gstdb.dbo.trafico_viaje
where no_viaje in (57032,57687)

