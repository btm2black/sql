-- =========================================================================== --
-- Creacion de Filtros en Consulta previa
-- Para Dpto Fiscal
-- =========================================================================== --


use integraapp
			
IF OBJECT_ID ('Gst_CxPFiscalX', 'V') IS NOT NULL
    DROP VIEW Gst_CxPFiscalX;
create view Gst_CxPFiscalX
with encryption
as
select
	 "fiscal".PerPost
	,"fiscal".Lote
	,"fiscal".Compañia
	,"fiscal".Monto
	,"fiscal".Iva
	,"fiscal".Base
	,"fiscal".RetISRHon as 'RetensionISR'
	,"fiscal".RetIVAHon as 'RetensionFlete'
	,"fiscal".RFC
	,"fiscal".UUID
	,"fiscal".Proveedor
	,"fiscal".Factura
	,"fiscal".OrdendeCompra
	,"fiscal".Descripción
	,"fiscal".DocType
from
	"integraapp"."dbo"."Gst_CxPFiscal" as "fiscal"
where
	"fiscal".UUID is not null
and 
--	SUBSTRING("fiscal".PerPost, 1,4) = '2018' -- 4.8 sec
	cast(substring("fiscal".PerPost, 1, 4) as int) = 2018 -- 1 sec
	
y
	
	
	select * from integraapp.dbo.Gst_CxPFiscalX
	
	
	exec sp_helptext Gst_CxPFiscal
	
use integraapp
 IF OBJECT_ID ('Gst_CxPFiscal', 'V') IS NOT NULL
    DROP VIEW Gst_CxPFiscal;
create view Gst_CxPFiscal
as
select
	top (100) percent dbo.Batch.PerPost,
	dbo.Batch.BatNbr as Lote,
	dbo.Batch.CpnyID as Compañia,
	dbo.APDoc.CuryOrigDocAmt as Monto,
	dbo.APDoc.CuryTaxTot00 as 'Iva',
	dbo.APDoc.CuryTxblTot00 as 'Base',
	dbo.APDoc.CuryTaxTot01 as 'RetISRHon',
	dbo.APDoc.CuryTaxTot02 as 'RetIVAHon',
	dbo.APDoc.VendId as RFC,
	dbo.xCEEIDoc.UUID,
	dbo.Vendor.Name as Proveedor,
	dbo.APDoc.InvcNbr as Factura,
	dbo.APDoc.PONbr as OrdendeCompra,
	dbo.APDoc.User2 as Descripción,
	dbo.APDoc.DocType
from
	dbo.Batch
inner join dbo.APDoc
on
	dbo.Batch.BatNbr = dbo.APDoc.BatNbr
left outer join dbo.xCEEIDoc on
	dbo.Batch.BatNbr = dbo.xCEEIDoc.Batnbr
left outer join dbo.Vendor on
	dbo.APDoc.VendId = dbo.Vendor.VendId
where
	(dbo.Vendor.ClassID <> 'FUNEMP')
	and (dbo.Batch.Status = 'P')
	and (dbo.Batch.Module = 'AP')
	and (dbo.APDoc.DocType <> 'HC')
	and (dbo.Batch.PerPost > '201500')
	and dbo.Batch.BatNbr = '410768'
order by
	dbo.Batch.PerPost,
	Lote
	
	
	