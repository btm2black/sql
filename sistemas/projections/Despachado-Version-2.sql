-- ==================================================================================================================== --	
-- =================================      full indicators for bonampakdb	     ====================================== --
-- ==================================================================================================================== --

-- 

-- SELECT DISTINCT local_tcp_portFROM sys.dm_exec_connections WHERE local_tcp_port IS NOT NULL

use sistemas
IF OBJECT_ID ('projections_view_full_indicators_tbk_core_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_tbk_core_periods;
    
create view projections_view_full_indicators_tbk_core_periods
with encryption
as
	with guia_tbk as 
		(
			select
					1 as 'company'
					,"viaje".id_area
					,"viaje".no_viaje
					,"viaje".id_area_liq
					,"viaje".id_personal
					,"viaje".kms_real
					,"liquidacion".no_liquidacion
					,"liquidacion".fecha_liquidacion
					,"viaje".id_ruta
					,"viaje".no_ejes_viaje
					,"viaje".id_origen
					,"viaje".id_destino
					,"viaje".fecha_real_viaje as 'fecha-real-viaje'
					,"viaje".fecha_real_fin_viaje as 'fecha-real-fin-viaje'
					,"viaje".f_despachado as 'fecha-despachado'
					,cast("viaje".fecha_real_viaje as date) as 'fecha_real_viaje'
					,cast("viaje".fecha_real_fin_viaje as date) as 'fecha_real_fin_viaje'
					,cast("viaje".f_despachado as date) as 'f_despachado'
					,"viaje".lts_empresa
					,"viaje".ton_viaje
					,"viaje".id_unidad
					,"viaje".id_remolque1
					,"viaje".id_remolque2
					,"viaje".id_dolly
					,"viaje".id_ingreso
					,"viaje".status_viaje
					,"viaje".viajeactual
					,"viaje".no_tarjeta_llave
					,"viaje".id_configuracionviaje
					,"viaje".kms_viaje
					,"viaje".id_areaviaje
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_viaje, 112) , 1, 6 ) as int) as 'fecha_real_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_fin_viaje, 112) , 1, 6 ) as int) as 'fecha_real_fin_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'f_despachado_m'
					,day("viaje".f_despachado) as 'day'
					,upper(left("translation-desp".month_name,1)) + right("translation-desp".month_name,len("translation-desp".month_name) - 1) as 'mes-despacho'
-- guia
--					,"guia".id_area
					,"guia".no_guia
					,"guia".tipo_pago
					,"guia".status_guia
					,"guia".clasificacion_doc
					,"guia".fecha_guia as 'fecha-guia'
					,"guia".fecha_confirmacion as 'fecha-confirmacion' --- hir
					,"guia".fecha_ingreso as 'fecha-ingreso'
					,"guia".fecha_modifico
					,"guia".fecha_cancelacion as 'fecha-cancelacion'
					,"guia".fecha_contabilizado as 'fecha-contabilizado' -- hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_contabilizado, 112) , 1, 6 ) as int) as 'fecha_contabilizado_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_confirmacion, 112) , 1, 6 ) as int) as 'fecha_confirmacion_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'fecha_guia_m'
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_cancelacion_m'
					,cast("guia".fecha_contabilizado as date) as 'fecha_contabilizado'  -- hir
					,cast("guia".fecha_confirmacion as date) as 'fecha_confirmacion'  --hir
					,cast("guia".fecha_guia as date) as 'fecha_guia'
					,cast("guia".fecha_cancelacion as date) as 'fecha_cancelacion'
					,day("guia".fecha_guia) as 'gday'
					,"guia".convenido_tonelada
					,"guia".id_area_facturacion
					,"guia".id_cliente
					,"guia".id_personal as 'gid_personal'
--					,"guia".no_viaje
					,"guia".id_remitente
					,"guia".id_destinatario
					,"guia".kms_guia
					,"guia".flete
					,"guia".seguro
					,"guia".maniobras
					,"guia".autopistas
					,"guia".otros
					,"guia".subtotal
					,"guia".iva_guia
					,"guia".cobro_viaje_kms
					,"guia".tipo_doc
					,"guia".id_origen as 'gid_origen'
					,"guia".id_destino as 'gid_destino'
					,"guia".id_fraccion
					,case 
						when "guia".id_fraccion in (1,2,6)
							then 'GRANEL'
						else 
							'OTROS'
					end as 'projections_rp_definition'
					,"guia".plaza_emision
					,"guia".num_guia
					,"guia".no_carta
					,"guia".id_remolque1 as 'gid_remolque1'
					,"guia".id_unidad as 'gid_unidad'
					,"guia".no_remision
					,"guia".motivo_cancelacion
					,"guia".id_remolque2 as 'gid_remolque2'
					,"guia".prestamo
					,"guia".num_guia_asignado
					,"guia".no_deposito
					,"guia".id_ingreso as 'gid_ingreso'
					,"guia".personalnombre
					,"guia".tipo_facturacion
					,"guia".no_transferencia_cobranza
					,"guia".factor_iva
					,"guia".num_guiacancel
					,"guia".tipo_origen
					,"guia".no_poliza
					,"guia".monto_retencion
					,"guia".status_pago
					,"guia".monto_retenciontercero
					,"guia".sustituye_documento
					,"guia".monto_ivaflete
					,"guia".tipocambioconvenio
					,"guia".desc_flete
					,"guia".flete_bruto
					,"guia".id_iva
					,"guia".id_retencion
					,"guia".id_convenio
					,"guia".id_areaconvenio
					,"guia".id_tipo_operacion
					,"guia".no_kit
					,"guia".monto_ivadescto
					,"guia".monto_retdescto
					,"guia".monto_descto
					,"guia".periodo_facturacion
					,"guia".tipo_producto
					,"guia".num_guia_incentivo
					,"guia".facturado
					,"guia".id_modifico
					,"guia".kms_convenio
					,"guia".observacion
-- client , route , manto , liquidacion	
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,ruta.desc_ruta
					,cliente.nombre as 'cliente'
					,manto.id_flota
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								bonampakdb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = "guia".no_guia and "tren".id_area = "viaje".id_area
					 ) as 'peso-aceptado'
					,(
						select 
								sum(isnull("trend".peso,0) + isnull("trend".peso_estimado,0)) 
						from 
								bonampakdb.dbo.trafico_renglon_guia as "trend"
						where	
								"trend".no_guia = "guia".no_guia and "trend".id_area = "viaje".id_area
					 ) as 'peso-despachado'
					,(
						select 
								descripcion
						from
								bonampakdb.dbo.trafico_configuracionviaje as "trviaje"
						where
								"trviaje".id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								bonampakdb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								bonampakdb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								bonampakdb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							bonampakdb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
			from 
						bonampakdb.dbo.trafico_viaje as "viaje"
				left join 
						bonampakdb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join 
						bonampakdb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				inner join
						sistemas.dbo.generals_month_translations as "translation-desp"
					on
						month("viaje".f_despachado) = "translation-desp".month_num
-- One to one
				left join 
						bonampakdb.dbo.trafico_guia as "guia"
					on	
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
--					and 
--						guia.prestamo <> 'P'
					and 
						"guia".tipo_doc = 2 
--					and
						
--				inner join
--						bonampakdb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area
--
--				left join 
--						sistemas.dbo.projections_fraccion_groups as "fgrp"
--					on 
--						"fgrp".projections_corporations_id = 1 and "guia".id_fraccion = "fgrp".projections_id_fraccion
--- ===============================
				left join 
						bonampakdb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
--- ===============================
--						
				left join
						bonampakdb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month("guia".fecha_guia) = "translation".month_num
		)
	select 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia
--				,"guia".mes ,"guia".f_despachado ,"guia".cliente 
--				
				 "guia".company,"guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
--				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
--				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
--				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota
--				,"guia"."peso-aceptado","guia"."peso-despachado"
				,"guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota
				,case 
					when "guia".id_tipo_operacion = 12 and company = 1 and id_area = 2
						then 'LA PAZ'
					when "guia".id_area = 4 and company = 1 
						then 'MEXICALI'
					else
						"guia".area
				end as 'area'
				,"guia".fraccion
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								2 -- cancelada efecto zero
							else 
								1 -- keep this cancelation
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						2 -- cancelada efecto zero
					else	
						0 -- todo lo demas
				 end as 'FlagIsDisminution'
				,case 
					when 
						"guia".prestamo = 'P'
					then 
						1
				 	else
				 		0
				 end as 'FlagIsProvision'
--				,case 
--					when 
--						( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--					then 
--						0 
--					else 
--						"guia".kms_viaje
--				end as 'kms_viaje'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_viaje
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else "guia".kms_viaje
						end
				 end as 'kms_viaje'
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else "guia".kms_real
--				end as 'kms_real'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_real
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when 
								( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 
								0 
							else 
								"guia".kms_real
						end
				 end as 'kms_real'
				,case
					when 
						"guia".id_fraccion in (1,2,6)
					then
					-- add hir
--						case 
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else ("guia".kms_viaje)*2
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										("guia".kms_viaje)*2
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
									then 0 else ("guia".kms_viaje)*2
								end
						 end
					else
					-- and hir
--						case
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else "guia".kms_real
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
--										1 -- Disminucion al siguiente mes [Provision - Aceptadas en otros meses Cancelasdas en este Mes ]
--										case 
--											when -- check if is in prestamo
----												"guia".prestamo = 'N'
										"guia".kms_real
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
										then 0 
									else "guia".kms_real
								end
						 end
				end as 'kms'
---> add description				
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia"."peso-aceptado") as 'peso-aceptado' , sum("guia"."peso-despachado") as 'peso-despachado'
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion --,"guia".company 
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else 1
--				end as 'trip_count'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else 1
						end
				 end as 'trip_count'
				 ,0 as 'FlagIsNextMonth'
	from 
				guia_tbk as "guia" -- where "guia".fecha_guia_m = '201810'
	group by 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia 
--				,"guia".mes,"guia".f_despachado ,"guia".cliente
--				,"guia".kms_viaje,"guia".kms_real
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company
				 "guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota,"guia"."peso-aceptado","guia"."peso-despachado","guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota,"guia".area,"guia".fraccion
				,"guia".company
				
			
-- ==================================================================================================================== --	
-- =================================      full indicators for macuspanadb	     ====================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_indicators_atm_core_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_atm_core_periods;
    
create view projections_view_full_indicators_atm_core_periods
with encryption
as
	with guia_atm as 
		(
			select
					2 as 'company'
					,"viaje".id_area
					,"viaje".no_viaje
					,"viaje".id_area_liq
					,"viaje".id_personal
					,"viaje".kms_real
					,"liquidacion".no_liquidacion
					,"liquidacion".fecha_liquidacion
					,"viaje".id_ruta
					,"viaje".no_ejes_viaje
					,"viaje".id_origen
					,"viaje".id_destino
					,"viaje".fecha_real_viaje as 'fecha-real-viaje'
					,"viaje".fecha_real_fin_viaje as 'fecha-real-fin-viaje'
					,"viaje".f_despachado as 'fecha-despachado'
					,cast("viaje".fecha_real_viaje as date) as 'fecha_real_viaje'
					,cast("viaje".fecha_real_fin_viaje as date) as 'fecha_real_fin_viaje'
					,cast("viaje".f_despachado as date) as 'f_despachado'
					,"viaje".lts_empresa
					,"viaje".ton_viaje
					,"viaje".id_unidad
					,"viaje".id_remolque1
					,"viaje".id_remolque2
					,"viaje".id_dolly
					,"viaje".id_ingreso
					,"viaje".status_viaje
					,"viaje".viajeactual
					,"viaje".no_tarjeta_llave
					,"viaje".id_configuracionviaje
					,"viaje".kms_viaje
					,"viaje".id_areaviaje
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_viaje, 112) , 1, 6 ) as int) as 'fecha_real_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_fin_viaje, 112) , 1, 6 ) as int) as 'fecha_real_fin_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'f_despachado_m'
					,day("viaje".f_despachado) as 'day'
					,upper(left("translation-desp".month_name,1)) + right("translation-desp".month_name,len("translation-desp".month_name) - 1) as 'mes-despacho'
-- guia
--					,"guia".id_area
					,"guia".no_guia
					,"guia".tipo_pago
					,"guia".status_guia
					,"guia".clasificacion_doc
					,"guia".fecha_guia as 'fecha-guia'
					,"guia".fecha_confirmacion as 'fecha-confirmacion' --- hir
					,"guia".fecha_ingreso as 'fecha-ingreso'
					,"guia".fecha_modifico
					,"guia".fecha_cancelacion as 'fecha-cancelacion'
					,"guia".fecha_contabilizado as 'fecha-contabilizado' -- hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_contabilizado, 112) , 1, 6 ) as int) as 'fecha_contabilizado_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_confirmacion, 112) , 1, 6 ) as int) as 'fecha_confirmacion_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'fecha_guia_m'
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_cancelacion_m'
					,cast("guia".fecha_contabilizado as date) as 'fecha_contabilizado'  -- hir
					,cast("guia".fecha_confirmacion as date) as 'fecha_confirmacion'  --hir
					,cast("guia".fecha_guia as date) as 'fecha_guia'
					,cast("guia".fecha_cancelacion as date) as 'fecha_cancelacion'
					,day("guia".fecha_guia) as 'gday'
					,"guia".convenido_tonelada
					,"guia".id_area_facturacion
					,"guia".id_cliente
					,"guia".id_personal as 'gid_personal'
--					,"guia".no_viaje
					,"guia".id_remitente
					,"guia".id_destinatario
					,"guia".kms_guia
					,"guia".flete
					,"guia".seguro
					,"guia".maniobras
					,"guia".autopistas
					,"guia".otros
					,"guia".subtotal
					,"guia".iva_guia
					,"guia".cobro_viaje_kms
					,"guia".tipo_doc
					,"guia".id_origen as 'gid_origen'
					,"guia".id_destino as 'gid_destino'
					,"guia".id_fraccion
					,case 
						when "guia".id_fraccion in (1,2,6)
							then 'GRANEL'
						else 
							'OTROS'
					end as 'projections_rp_definition'
					,"guia".plaza_emision
					,"guia".num_guia
					,"guia".no_carta
					,"guia".id_remolque1 as 'gid_remolque1'
					,"guia".id_unidad as 'gid_unidad'
					,"guia".no_remision
					,"guia".motivo_cancelacion
					,"guia".id_remolque2 as 'gid_remolque2'
					,"guia".prestamo
					,"guia".num_guia_asignado
					,"guia".no_deposito
					,"guia".id_ingreso as 'gid_ingreso'
					,"guia".personalnombre
					,"guia".tipo_facturacion
					,"guia".no_transferencia_cobranza
					,"guia".factor_iva
					,"guia".num_guiacancel
					,"guia".tipo_origen
					,"guia".no_poliza
					,"guia".monto_retencion
					,"guia".status_pago
					,"guia".monto_retenciontercero
					,"guia".sustituye_documento
					,"guia".monto_ivaflete
					,"guia".tipocambioconvenio
					,"guia".desc_flete
					,"guia".flete_bruto
					,"guia".id_iva
					,"guia".id_retencion
					,"guia".id_convenio
					,"guia".id_areaconvenio
					,"guia".id_tipo_operacion
					,"guia".no_kit
					,"guia".monto_ivadescto
					,"guia".monto_retdescto
					,"guia".monto_descto
					,"guia".periodo_facturacion
					,"guia".tipo_producto
					,"guia".num_guia_incentivo
					,"guia".facturado
					,"guia".id_modifico
					,"guia".kms_convenio
					,'' as 'observacion'
-- client , route , manto , liquidacion	
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,ruta.desc_ruta
					,cliente.nombre as 'cliente'
					,manto.id_flota
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								macuspanadb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = "guia".no_guia and "tren".id_area = "viaje".id_area
					 ) as 'peso-aceptado'
					,(
						select 
								sum(isnull("trend".peso,0) + isnull("trend".peso_estimado,0)) 
						from 
								macuspanadb.dbo.trafico_renglon_guia as "trend"
						where	
								"trend".no_guia = "guia".no_guia and "trend".id_area = "viaje".id_area
					 ) as 'peso-despachado'
					,(
						select 
								descripcion
						from
								macuspanadb.dbo.trafico_configuracionviaje as "trviaje"
						where
								"trviaje".id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								macuspanadb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								macuspanadb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								macuspanadb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							macuspanadb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
			from 
						macuspanadb.dbo.trafico_viaje as "viaje"
				left join 
						macuspanadb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join 
						macuspanadb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				inner join
						sistemas.dbo.generals_month_translations as "translation-desp"
					on
						month("viaje".f_despachado) = "translation-desp".month_num
-- One to one
				left join 
						macuspanadb.dbo.trafico_guia as "guia"
					on	
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
--					and 
--						guia.prestamo <> 'P'
					and 
						"guia".tipo_doc = 2 
--					and
						
--				inner join
--						macuspanadb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area
--
--				left join 
--						sistemas.dbo.projections_fraccion_groups as "fgrp"
--					on 
--						"fgrp".projections_corporations_id = 1 and "guia".id_fraccion = "fgrp".projections_id_fraccion
--- ===============================
				left join 
						bonampakdb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
--- ===============================
--						
				left join
						macuspanadb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month("guia".fecha_guia) = "translation".month_num
		)
	select 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia
--				,"guia".mes ,"guia".f_despachado ,"guia".cliente 
--				
				 "guia".company,"guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
--				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
--				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
--				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota
--				,"guia"."peso-aceptado","guia"."peso-despachado"
				,"guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota
				,case 
					when "guia".id_tipo_operacion = 12 and company = 1 and id_area = 2
						then 'LA PAZ'
					when "guia".id_area = 4 and company = 1 
						then 'MEXICALI'
					else
						"guia".area
				end as 'area'
				,"guia".fraccion
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								2 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						2 -- cancelada efecto zero
					else	
						0 -- todo lo demas
				 end as 'FlagIsDisminution'
				,case 
					when 
						"guia".prestamo = 'P'
					then 
						1
				 	else
				 		0
				 end as 'FlagIsProvision'
--				,case 
--					when 
--						( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--					then 
--						0 
--					else 
--						"guia".kms_viaje
--				end as 'kms_viaje'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_viaje
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else "guia".kms_viaje
						end
				 end as 'kms_viaje'
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else "guia".kms_real
--				end as 'kms_real'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_real
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else "guia".kms_real
						end
				 end as 'kms_real'
				,case
					when 
						"guia".id_fraccion in (1,2,6)
					then
					-- add hir
--						case 
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else ("guia".kms_viaje)*2
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										("guia".kms_viaje)*2
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
									then 0 else ("guia".kms_viaje)*2
								end
						 end
					else
					-- and hir
--						case
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else "guia".kms_real
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										"guia".kms_real
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
										then 0 
									else "guia".kms_real
								end
						 end
				end as 'kms'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia"."peso-aceptado") as 'peso-aceptado' , sum("guia"."peso-despachado") as 'peso-despachado'
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion --,"guia".company 
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else 1
--				end as 'trip_count'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else 1
						end
				 end as 'trip_count'
				 ,0 as 'FlagIsNextMonth'
	from 
				guia_atm as "guia" -- where "guia".fecha_guia_m = '201810'
	group by 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia 
--				,"guia".mes,"guia".f_despachado ,"guia".cliente
--				,"guia".kms_viaje,"guia".kms_real
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company
				 "guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota,"guia"."peso-aceptado","guia"."peso-despachado","guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota,"guia".area,"guia".fraccion
				,"guia".company				
				
-- ==================================================================================================================== --	
-- =================================      full indicators for tespecializadadb   ====================================== --
-- ==================================================================================================================== --


use sistemas
IF OBJECT_ID ('projections_view_full_indicators_tei_core_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_tei_core_periods;
    
create view projections_view_full_indicators_tei_core_periods
with encryption
as
	with guia_tei as 
		(
			select
					 3 as 'company'
					,"viaje".id_area
					,"viaje".no_viaje
					,"viaje".id_area_liq
					,"viaje".id_personal
					,"viaje".kms_real
					,"liquidacion".no_liquidacion
					,"liquidacion".fecha_liquidacion
					,"viaje".id_ruta
					,"viaje".no_ejes_viaje
					,"viaje".id_origen
					,"viaje".id_destino
					,"viaje".fecha_real_viaje as 'fecha-real-viaje'
					,"viaje".fecha_real_fin_viaje as 'fecha-real-fin-viaje'
					,"viaje".f_despachado as 'fecha-despachado'
					,cast("viaje".fecha_real_viaje as date) as 'fecha_real_viaje'
					,cast("viaje".fecha_real_fin_viaje as date) as 'fecha_real_fin_viaje'
					,cast("viaje".f_despachado as date) as 'f_despachado'
					,"viaje".lts_empresa
					,"viaje".ton_viaje
					,"viaje".id_unidad
					,"viaje".id_remolque1
					,"viaje".id_remolque2
					,"viaje".id_dolly
					,"viaje".id_ingreso
					,"viaje".status_viaje
					,"viaje".viajeactual
					,"viaje".no_tarjeta_llave
					,"viaje".id_configuracionviaje
					,"viaje".kms_viaje
					,"viaje".id_areaviaje
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_viaje, 112) , 1, 6 ) as int) as 'fecha_real_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_fin_viaje, 112) , 1, 6 ) as int) as 'fecha_real_fin_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'f_despachado_m'
					,day("viaje".f_despachado) as 'day'
					,upper(left("translation-desp".month_name,1)) + right("translation-desp".month_name,len("translation-desp".month_name) - 1) as 'mes-despacho'
-- guia
--					,"guia".id_area
					,"guia".no_guia
					,"guia".tipo_pago
					,"guia".status_guia
					,"guia".clasificacion_doc
					,"guia".fecha_guia as 'fecha-guia'
					,"guia".fecha_confirmacion as 'fecha-confirmacion' --- hir
					,"guia".fecha_ingreso as 'fecha-ingreso'
					,"guia".fecha_modifico
					,"guia".fecha_cancelacion as 'fecha-cancelacion'
					,"guia".fecha_contabilizado as 'fecha-contabilizado' -- hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_contabilizado, 112) , 1, 6 ) as int) as 'fecha_contabilizado_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_confirmacion, 112) , 1, 6 ) as int) as 'fecha_confirmacion_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'fecha_guia_m'
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_cancelacion_m'
					,cast("guia".fecha_contabilizado as date) as 'fecha_contabilizado'  -- hir
					,cast("guia".fecha_confirmacion as date) as 'fecha_confirmacion'  --hir
					,cast("guia".fecha_guia as date) as 'fecha_guia'
					,cast("guia".fecha_cancelacion as date) as 'fecha_cancelacion'
					,day("guia".fecha_guia) as 'gday'
					,"guia".convenido_tonelada
					,"guia".id_area_facturacion
					,"guia".id_cliente
					,"guia".id_personal as 'gid_personal'
--					,"guia".no_viaje
					,"guia".id_remitente
					,"guia".id_destinatario
					,"guia".kms_guia
					,"guia".flete
					,"guia".seguro
					,"guia".maniobras
					,"guia".autopistas
					,"guia".otros
					,"guia".subtotal
					,"guia".iva_guia
					,"guia".cobro_viaje_kms
					,"guia".tipo_doc
					,"guia".id_origen as 'gid_origen'
					,"guia".id_destino as 'gid_destino'
					,"guia".id_fraccion
					,case 
						when "guia".id_fraccion in (1,2,6)
							then 'GRANEL'
						else 
							'OTROS'
					end as 'projections_rp_definition'
					,"guia".plaza_emision
					,"guia".num_guia
					,"guia".no_carta
					,"guia".id_remolque1 as 'gid_remolque1'
					,"guia".id_unidad as 'gid_unidad'
					,"guia".no_remision
					,"guia".motivo_cancelacion
					,"guia".id_remolque2 as 'gid_remolque2'
					,"guia".prestamo
					,"guia".num_guia_asignado
					,"guia".no_deposito
					,"guia".id_ingreso as 'gid_ingreso'
					,"guia".personalnombre
					,"guia".tipo_facturacion
					,"guia".no_transferencia_cobranza
					,"guia".factor_iva
					,"guia".num_guiacancel
					,"guia".tipo_origen
					,"guia".no_poliza
					,"guia".monto_retencion
					,"guia".status_pago
					,"guia".monto_retenciontercero
					,"guia".sustituye_documento
					,"guia".monto_ivaflete
					,"guia".tipocambioconvenio
					,"guia".desc_flete
					,"guia".flete_bruto
					,"guia".id_iva
					,"guia".id_retencion
					,"guia".id_convenio
					,"guia".id_areaconvenio
					,"guia".id_tipo_operacion
					,"guia".no_kit
					,"guia".monto_ivadescto
					,"guia".monto_retdescto
					,"guia".monto_descto
					,"guia".periodo_facturacion
					,"guia".tipo_producto
					,"guia".num_guia_incentivo
					,"guia".facturado
					,"guia".id_modifico
					,"guia".kms_convenio
					,'' as 'observacion'
-- client , route , manto , liquidacion	
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,ruta.desc_ruta
					,cliente.nombre as 'cliente'
					,manto.id_flota
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								tespecializadadb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = "guia".no_guia and "tren".id_area = "viaje".id_area
					 ) as 'peso-aceptado'
					,(
						select 
								sum(isnull("trend".peso,0) + isnull("trend".peso_estimado,0)) 
						from 
								tespecializadadb.dbo.trafico_renglon_guia as "trend"
						where	
								"trend".no_guia = "guia".no_guia and "trend".id_area = "viaje".id_area
					 ) as 'peso-despachado'
					,(
						select 
								descripcion
						from
								tespecializadadb.dbo.trafico_configuracionviaje as "trviaje"
						where
								"trviaje".id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								tespecializadadb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								tespecializadadb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								tespecializadadb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							tespecializadadb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
			from 
						tespecializadadb.dbo.trafico_viaje as "viaje"
				left join 
						tespecializadadb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join 
						tespecializadadb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				inner join
						sistemas.dbo.generals_month_translations as "translation-desp"
					on
						month("viaje".f_despachado) = "translation-desp".month_num
-- One to one
				left join 
						tespecializadadb.dbo.trafico_guia as "guia"
					on	
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
--					and 
--						guia.prestamo <> 'P'
					and 
						"guia".tipo_doc = 2 
--					and
						
--				inner join
--						tespecializadadb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area
--
--				left join 
--						sistemas.dbo.projections_fraccion_groups as "fgrp"
--					on 
--						"fgrp".projections_corporations_id = 1 and "guia".id_fraccion = "fgrp".projections_id_fraccion
--- ===============================
				left join 
						tespecializadadb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
--- ===============================
--						
				left join
						tespecializadadb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month("guia".fecha_guia) = "translation".month_num
		)
	select 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia
--				,"guia".mes ,"guia".f_despachado ,"guia".cliente 
--				
				 "guia".company,"guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
--				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
--				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
--				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota
--				,"guia"."peso-aceptado","guia"."peso-despachado"
				,"guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota
				,case 
					when "guia".id_tipo_operacion = 12 and company = 1 and id_area = 2
						then 'LA PAZ'
					when "guia".id_area = 4 and company = 1 
						then 'MEXICALI'
					else
						"guia".area
				end as 'area'
				,"guia".fraccion
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								2 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						2 -- cancelada efecto zero
					else	
						0 -- todo lo demas
				 end as 'FlagIsDisminution'
				,case 
					when 
						"guia".prestamo = 'P'
					then 
						1
				 	else
				 		0
				 end as 'FlagIsProvision'
--				,case 
--					when 
--						( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--					then 
--						0 
--					else 
--						"guia".kms_viaje
--				end as 'kms_viaje'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_viaje
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else "guia".kms_viaje
						end
				 end as 'kms_viaje'
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else "guia".kms_real
--				end as 'kms_real'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_real
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
								then 
									0 
							else 
								"guia".kms_real
						end
				 end as 'kms_real'
				,case
					when 
						"guia".id_fraccion in (1,2,6)
					then
					-- add hir
--						case 
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else ("guia".kms_viaje)*2
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										("guia".kms_viaje)*2
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
									then 0 else ("guia".kms_viaje)*2
								end
						 end
					else
					-- and hir
--						case
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else "guia".kms_real
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
										"guia".fecha_confirmacion_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										"guia".kms_real --NOTE Add a tag field for querys
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
										then 0 
									else "guia".kms_real -- NOTE Add a tag field for querys
								end
						 end
				end as 'kms'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia"."peso-aceptado") as 'peso-aceptado' , sum("guia"."peso-despachado") as 'peso-despachado'
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion --,"guia".company 
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else 1
--				end as 'trip_count'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
								"guia".fecha_confirmacion_m is null
							then
								0 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else 1
						end
				 end as 'trip_count'
				 ,0 as 'FlagIsNextMonth'
	from 
				guia_tei as "guia" -- where "guia".fecha_guia_m = '201810'
	group by 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia 
--				,"guia".mes,"guia".f_despachado ,"guia".cliente
--				,"guia".kms_viaje,"guia".kms_real
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company
				 "guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
				,"guia".kms_viaje
				,"guia".id_areaviaje,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho","guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".id_remitente,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino,"guia".id_fraccion
				,"guia".projections_rp_definition
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente,"guia".id_flota,"guia"."peso-aceptado","guia"."peso-despachado","guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota,"guia".area,"guia".fraccion
				,"guia".company	
				
				
				
				
				
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company    ======================================== --
-- ==================================================================================================================== --
-- Build for general core
-- select * from projections_view_full_company_core_indicators
				
				
use sistemas
IF OBJECT_ID ('projections_view_full_company_core_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_core_indicators;
    
create view projections_view_full_company_core_indicators
as 
(
	select 
			 *
	from 
			sistemas.dbo.projections_view_full_indicators_tbk_core_periods
	union all
	select 
			 *
	from 
			sistemas.dbo.projections_view_full_indicators_atm_core_periods
	union all
	select 
			*
	from 
			sistemas.dbo.projections_view_full_indicators_tei_core_periods
) 
	
				
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company    ======================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_company_discore_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_discore_indicators;
    
create view projections_view_full_company_discore_indicators
as 				
select 
		 "dissmiss".company
		,"dissmiss".id_area
		,"dissmiss".no_viaje
		,"dissmiss".id_area_liq
		,"dissmiss".id_personal
		,"dissmiss".no_liquidacion
		,"dissmiss".fecha_liquidacion
		,"dissmiss".id_ruta
		,"dissmiss".no_ejes_viaje
		,"dissmiss".id_origen
		,"dissmiss".id_destino
		,"dissmiss".[fecha-real-viaje]
		,"dissmiss".[fecha-real-fin-viaje]
		,"dissmiss".[fecha-despachado]
		,"dissmiss".fecha_real_viaje
		,"dissmiss".fecha_real_fin_viaje
		,"dissmiss".f_despachado
		,"dissmiss".lts_empresa
		,"dissmiss".ton_viaje
		,"dissmiss".id_unidad
		,"dissmiss".id_remolque1
		,"dissmiss".id_remolque2
		,"dissmiss".id_dolly
		,"dissmiss".id_ingreso
		,"dissmiss".status_viaje
		,"dissmiss".viajeactual
		,"dissmiss".no_tarjeta_llave
		,"dissmiss".id_configuracionviaje
		,"dissmiss".id_areaviaje
		,"dissmiss".fecha_real_viaje_m
		,"dissmiss".fecha_real_fin_viaje_m
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int)  as 'f_despachado_m'
		,"dissmiss".[day]
		,"dissmiss".[mes-despacho]
		,"dissmiss".no_guia
		,"dissmiss".tipo_pago
		,"dissmiss".status_guia
		,"dissmiss".clasificacion_doc
		,"dissmiss".[fecha-guia]
		,"dissmiss".[fecha-confirmacion]
		,"dissmiss".[fecha-ingreso]
		,"dissmiss".fecha_modifico
		,"dissmiss".[fecha-cancelacion]
		,"dissmiss".[fecha-contabilizado]
		,"dissmiss".fecha_contabilizado_m
		,"dissmiss".fecha_confirmacion_m
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int)  as 'fecha_guia_m'
		,"dissmiss".fecha_cancelacion_m
		,"dissmiss".fecha_contabilizado
		,"dissmiss".fecha_confirmacion
		,"dissmiss".fecha_guia
		,"dissmiss".fecha_cancelacion
		,"dissmiss".gday
		,"dissmiss".convenido_tonelada
		,"dissmiss".id_area_facturacion
		,"dissmiss".id_cliente
		,"dissmiss".gid_personal
		,"dissmiss".id_remitente
		,"dissmiss".id_destinatario
		,"dissmiss".kms_guia
		,"dissmiss".flete
		,"dissmiss".seguro
		,"dissmiss".maniobras
		,"dissmiss".autopistas
		,"dissmiss".otros
		,"dissmiss".iva_guia
		,"dissmiss".cobro_viaje_kms
		,"dissmiss".tipo_doc
		,"dissmiss".gid_origen
		,"dissmiss".gid_destino
		,"dissmiss".id_fraccion
		,"dissmiss".projections_rp_definition
		,"dissmiss".plaza_emision
		,"dissmiss".num_guia
		,"dissmiss".no_carta
		,"dissmiss".gid_remolque1
		,"dissmiss".gid_unidad
		,"dissmiss".no_remision
		,"dissmiss".motivo_cancelacion
		,"dissmiss".gid_remolque2
		,"dissmiss".prestamo
		,"dissmiss".num_guia_asignado
		,"dissmiss".no_deposito
		,"dissmiss".gid_ingreso
		,"dissmiss".personalnombre
		,"dissmiss".tipo_facturacion
		,"dissmiss".no_transferencia_cobranza
		,"dissmiss".factor_iva
		,"dissmiss".num_guiacancel
		,"dissmiss".tipo_origen
		,"dissmiss".no_poliza
		,"dissmiss".monto_retencion
		,"dissmiss".status_pago
		,"dissmiss".monto_retenciontercero
		,"dissmiss".sustituye_documento
		,"dissmiss".monto_ivaflete
		,"dissmiss".tipocambioconvenio
		,"dissmiss".desc_flete
		,"dissmiss".flete_bruto
		,"dissmiss".id_iva
		,"dissmiss".id_retencion
		,"dissmiss".id_convenio
		,"dissmiss".id_areaconvenio
		,"dissmiss".id_tipo_operacion
		,"dissmiss".no_kit
		,"dissmiss".monto_ivadescto
		,"dissmiss".monto_retdescto
		,"dissmiss".monto_descto
		,"dissmiss".periodo_facturacion
		,"dissmiss".tipo_producto
		,"dissmiss".num_guia_incentivo
		,"dissmiss".facturado
		,"dissmiss".id_modifico
		,"dissmiss".kms_convenio
		,"dissmiss".observacion
		,"dissmiss".mes
		,"dissmiss".desc_ruta
		,"dissmiss".cliente
		,"dissmiss".id_flota
		,"dissmiss".configuracion_viaje
		,"dissmiss".tipo_de_operacion
		,"dissmiss".flota
		,"dissmiss".area
		,"dissmiss".fraccion
		,"dissmiss".FlagIsDisminution
		,"dissmiss".FlagIsProvision
		,"dissmiss".kms_viaje
		,"dissmiss".kms_real
		,"dissmiss".kms*-1 as 'kms'
		,"dissmiss".subtotal*-1 as 'subtotal'
		,"dissmiss".[peso-aceptado]*-1 as 'peso-aceptado'
		,"dissmiss".[peso-despachado]*-1 as 'peso-despachado'
		,"dissmiss".trip_count*-1 as 'trip_count'
		,1 as 'FlagIsNextMonth'
from
		sistemas.dbo.projections_view_full_company_core_indicators as "dissmiss"
where 
		FlagIsDisminution = 1		
		
		
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company test    ======================================== --
-- ==================================================================================================================== --
-- Build for general core
-- projections_view_full_company_core_indicators
				
				
use sistemas
IF OBJECT_ID ('projections_view_full_company_testcore_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_testcore_indicators;
    
create view projections_view_full_company_testcore_indicators
as 
(
	select 
			 *
	from 
			sistemas.dbo.projections_view_full_company_core_indicators
	union all
	select 
			 *
	from 
			sistemas.dbo.projections_view_full_company_discore_indicators
) 
	

	
	
--<---------------------------------------------------->
-- ====================================================================================================== --
-- ==============   Start Query About 'Despachado' Indicators with the new standart in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability
use sistemas
IF OBJECT_ID ('projections_view_full_company_core_desp_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_core_desp_indicators;
    
create view projections_view_full_company_core_desp_indicators
with encryption
as 				
--with "despachado" as (
						select --row_number() over (order by no_guia) as 'id',
								 "desp".company
								,"desp".id_area
								,"desp".area
								,"desp".id_fraccion
								,"desp".fraccion
								,year("desp".f_despachado) as 'dyear'
								,year("desp".fecha_guia) as 'ayear'
								,"desp".[mes-despacho] as 'dmes'
								,"desp".mes as 'ames'
								,"desp".kms as 'kms'
								,"desp".subtotal as 'subtotal'
								,"desp".[peso-despachado] as 'dpeso'
								,"desp".[peso-aceptado] as 'apeso'
								,"desp".trip_count as 'non-zero'
								,"desp".f_despachado_m
								,"desp".fecha_guia_m
								,"desp".projections_rp_definition
								,"desp".FlagIsDisminution
								,"desp".FlagIsProvision
								,"desp".FlagIsNextMonth
						from
								sistemas.dbo.projections_view_full_company_testcore_indicators as "desp"
						where
								"desp".FlagIsDisminution <> 2 --> aceptado + canceladas
						and 
								"desp".no_guia is not null
						and 
							(
								cast(left("desp".f_despachado_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							or
								cast(left("desp".fecha_guia_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							)
--						group by 
--								 "desp".company
--								,"desp".id_area
--								,"desp".area
--								,"desp".id_fraccion
--								,"desp".fraccion
--								,year("desp".f_despachado)
--								,year("desp".fecha_guia)
--								,"desp".[mes-despacho]
--								,"desp".mes
--								,"desp".f_despachado_m
--								,"desp".fecha_guia_m
--								,"desp".FlagIsDisminution
--								,"desp".FlagIsProvision
--)
--select 
--	 row_number() over (order by id_area) as 'id'
--	,company
--	,id_area
--	,area
--	,id_fraccion
--	,fraccion
--	,dyear
--	,ayear
--	,dmes
--	,ames
--	,kms
--	,subtotal
--	,dpeso
--	,apeso
--	,[non-zero]
--	,f_despachado_m
--	,fecha_guia_m
--	,FlagIsDisminution
--	,FlagIsProvision
--from "despachado" -- left joint to areas ??
--group by ...

							
							

-- ====================================================================================================== --
-- ==============   Query for Despachado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability

-- source select * from sistemas.dbo.projections_view_full_company_core_desp_indicators
-- Replace with old query select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops
-- SRC: select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
use sistemas
IF OBJECT_ID ('projections_view_indicators_dispatch_periods_full_first_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_dispatch_periods_full_first_ops;
    
create view projections_view_indicators_dispatch_periods_full_first_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,"desp".area
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".f_despachado_m,4) as int) as 'cyear'
		,"month".month_uname as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subtotal'
		,sum("desp".dpeso) as 'peso'
		,sum("desp".[non-zero]) as 'non_zero'
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".f_despachado_m
from 
		sistemas.dbo.projections_view_full_company_core_desp_indicators as "desp"
inner join 
		sistemas.dbo.generals_month_translations as "month"
	on
		"month".month_num = cast(right("desp".f_despachado_m,2) as int)	
where 
		cast(left("desp".f_despachado_m,4) as int) in ( year(dateadd(yy,-1,current_timestamp)) , year(current_timestamp) )
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".f_despachado_m,4) as int)
		,"month".month_uname
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".f_despachado_m

	
		
--		update integraapp.dbo.GLTran
--		set FiscYr = left(perpost,4) 
		select FiscYr,left(perpost,4) as 'per' from integraapp.dbo.GLTran
		where
		FiscYr is null or FiscYr = ''
		
		
		
		
		select left(201810,4) as perpoost

-- ====================================================================================================== --
-- ==============   Query for Despachado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- SRC: select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
-- select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops
-- select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_ops
use sistemas
IF OBJECT_ID ('projections_view_indicators_dispatch_periods_full_src_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_dispatch_periods_full_src_ops
    
create view projections_view_indicators_dispatch_periods_full_src_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,case 
			when rtrim(ltrim("desp".area)) = 'MEXICALI'
				then 'TIJUANA'
			else
				rtrim(ltrim("desp".area))
		end as 'area'
		,"desp".id_fraccion
		,rtrim(ltrim("desp".fraccion)) as 'fraccion'
		,"desp".cyear
		,rtrim(ltrim("desp".mes)) as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subtotal'
		,sum("desp".peso) as 'peso'
		,sum("desp".[non-zero]) as 'non_zero'
from 
		sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops as "desp"
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
		,"desp".id_fraccion
		,"desp".fraccion
		,"desp".cyear
		,"desp".mes
		
		
		
		
	-- ==== Triggers Control 
	
--Existen registros nuevos en la base de dato o en las bases de datos? 
--cuales registros se necesitan revisar? trafico_viaje[Viaje despachado] y trafico_guia [Canceladas y Aceptadas]
--
--		triggers options
--		 
--		 insert 
--		 		[
--		 			 trafico_viaje[viaje_desp][CreaNuevo] => [true ,false ][timeMark]
--		 			,trafico_guia[Cartaporte][CreaNuevo] => [true ,false ][timeMark]
--		 		] 
--		 
--		 update 
--					 trafico_guia[Cartaporte][Edita] => [true ,false ][timeMark]
--		 process
--		 		[
--		 			execute sp for update { first record in table time and stats } 
--		 		]	 		
--		
--		build table :
		

--========================================================================================================================================= --
-- 											Projections_Systems_Logs																		--
--========================================================================================================================================= --

-- Procedure Check

Select * from
				(
					SELECT * FROM sys.dm_exec_requests 
					where sql_handle is not null
				) a 
	CROSS APPLY  sys.dm_exec_sql_text(a.sql_handle) t 
	where t.text like 'CREATE PROCEDURE dbo.sp_sleeping_beauty%'


-- monitor		
select cast (created as date) as 'date', cast(created as time) as 'time' , ltrim(rtrim(database_record)) as 'database_record', mode_crud from sistemas.dbo.projections_bridge_ux_systems_logs where mode_crud in (1,3) and status = 1 order by created

-- create a job
use msdb
exec dbo.sp_add_job_quick 
@job = 'ProjectionsUpdate', -- The job name
@mycommand = 'exec sistemas.dbo.sp_projections_indicadores_operativos', -- The T-SQL command to run in the step
@servername = 'INTEGRABD', -- SQL Server name. If running localy, you can use @servername=@@Servername
@startdate = '20181206', -- The date August 29th, 2013
@starttime = '192700' -- The time, 19:27:00

--select @@Servername
exec sistemas.dbo.sp_projections_indicadores_operativos

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops

use [sistemas]
-- go
IF OBJECT_ID('projections_bridge_ux_systems_logs', 'U') IS NOT NULL 
  DROP TABLE projections_bridge_ux_systems_logs 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table [dbo].[projections_bridge_ux_systems_logs](
		id							int identity(1,1),
		custom_id					int,
		database_record				char(150),
		table_record				char(150),
		mode_crud					tinyint, -- 1=c,2=r,3=u,4=d
		data_crud					text,
		created						datetime,
		modified					datetime,
		status						tinyint default 1 null,
) on [primary]
-- go
set ansi_padding off
-- go		
		 	

--========================================================================================================================================= --



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table projections_view_indicators_dispatch_periods_full_src_tbl_ops(

									 id					 int 			identity(1,1)
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,f_despachado_m		 int			null
									,created			 datetime		null
									,modified			 datetime		null
									,status				 tinyint default 1 null
) on [primary]
-- go
set ansi_padding off
-- go



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table projections_view_indicators_acepted_periods_full_src_tbl_ops(

									 id					 int 			identity(1,1)
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,fecha_guia_m		 int			null
									,created			 datetime		null
									,modified			 datetime		null
									,status				 tinyint default 1 null
) on [primary]
-- go
set ansi_padding off
-- go


-- ========================================================================================================================================= --
-- The Procedure every 5 mins or 3 perhaps ??
-- ========================================================================================================================================= --
use sistemas
ALTER procedure [dbo].[sp_projections_indicadores_operativos]
as

	declare @temp_table table ( -- despachado
									 id					 int			null
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,f_despachado_m		 int			null
	)
	
	declare @acoe_table table ( -- aceptado
									 id					 int			null
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,fecha_guia_m		 int			null
	)
		

	insert into @temp_table
		select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops
		
--	select * from @temp_table
		
	truncate table sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
--	print 'update table ' + cast(getdate() as varchar(max))
	insert into sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
		select 
				 "src".company
				,"src".id_area
				,"src".area
				,"src".tname
				,"src".id_fraccion
				,"src".fraccion
				,"src".cyear
				,"src".mes
				,"src".kms
				,"src".subtotal
				,"src".peso
				,"src"."non-zero"
				,"src".FlagIsDisminution
				,"src".FlagIsProvision
				,"src".FlagIsNextMonth
				,"src".f_despachado_m
				,current_timestamp
				,current_timestamp
				,1
		from @temp_table as "src"

-- add acepted row
	insert into @acoe_table
		select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_first_ops  -- moded
		
--	select * from @temp_table
		
	truncate table sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops  -- moded
--	print 'update table ' + cast(getdate() as varchar(max))
	insert into sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops  --moded
		select 
				 "srca".company
				,"srca".id_area
				,"srca".area
				,"srca".tname
				,"srca".id_fraccion
				,"srca".fraccion
				,"srca".cyear
				,"srca".mes
				,"srca".kms
				,"srca".subtotal
				,"srca".peso
				,"srca"."non-zero"
				,"srca".FlagIsDisminution
				,"srca".FlagIsProvision
				,"srca".FlagIsNextMonth
				,"srca".fecha_guia_m
				,current_timestamp
				,current_timestamp
				,1
		from @acoe_table as "srca"	
		
		
--========================================================================================================================================= --

		
--========================================================================================================================================= --
-- 											Triggers for projections																		--
--========================================================================================================================================= --
use bonampakdb
-- drop trigger monitor_ux_insert
create trigger monitor_ux_insert
ON bonampakdb.dbo.trafico_viaje
with encryption
after insert
as 
begin
	-- Catch the rows for examination
	set nocount on
	-- then save in statistics
	insert into sistemas.dbo.projections_bridge_ux_systems_logs
		select 
--				 null,
				 1 -- custom_id 
				,'bonampakdb'
				,'bonampakdb.dbo.trafico_viaje'
				,1 -- 1=c,2=r,3=u,4=d
				,'[1]['+ cast("i".no_viaje as char(10)) + '][' + cast("i".id_area as char(1)) + ']' -- data_crud
				,current_timestamp
				,current_timestamp
				,1
		from 
				inserted as "i"
	-- Run sp
end
	--========================================================================================================================================= --
-- Macuspana
use macuspanadb
-- drop trigger monitor_ux_insert
create trigger monitor_ux_insert
ON macuspanadb.dbo.trafico_viaje
with encryption
after insert
as 
begin
	-- Catch the rows for examination
	set nocount on
	-- then save in statistics
	insert into sistemas.dbo.projections_bridge_ux_systems_logs
		select 
--				 null,
				 1  -- custom_id 
				,'macuspanadb'
				,'macuspanadb.dbo.trafico_viaje'
				,1 -- 1=c,2=r,3=u,4=d
				,'[2][' + cast("i".no_viaje as char(10)) + '][' + cast("i".id_area as char(1)) + ']' -- data_crud
				,current_timestamp
				,current_timestamp
				,1
		from 
				inserted as "i"
	-- Run sp
end
	--========================================================================================================================================= --
-- Teisa
use tespecializadadb
-- drop trigger monitor_ux_insert
create trigger monitor_ux_insert
ON tespecializadadb.dbo.trafico_viaje
with encryption
after insert
as 
begin
	-- Catch the rows for examination
	set nocount on
	-- then save in statistics
	insert into sistemas.dbo.projections_bridge_ux_systems_logs
		select 
--				 null,
				 1  -- custom_id 
				,'tespecializadadb'
				,'tespecializadadb.dbo.trafico_viaje'
				,1 -- 1=c,2=r,3=u,4=d
				,'[3][' + cast("i".no_viaje as char(10)) + '][' + cast("i".id_area as char(1)) + ']' -- data_crud
				,current_timestamp
				,current_timestamp
				,1
		from 
				inserted as "i"
	-- Run sp
end
--========================================================================================================================================= --
-- Insert and Update in trafico_viaje and trafico_guia 
--========================================================================================================================================= --
use bonampakdb
-- drop trigger monitor_ux_insert_update
create trigger monitor_ux_insert_update
ON bonampakdb.dbo.trafico_guia
with encryption
after insert,update
as 
begin
	-- Catch the rows for examination
	set nocount on
	
	declare  @action as int
	
	if  COLUMNS_UPDATED() <> 0 -- delete or update?
		begin 
		  if exists  (select * from deleted) -- updated cols + old rows means action=update       
		    set @action = 3  -- UPDATE
		  else 
		  	set @action = 1  -- 'INSERT' -- updated columns and nothing deleted means action=insert
		end 
	else -- delete     
		begin 
			set @action = 4	 -- 'DELETE'
		end 
	
		insert into sistemas.dbo.projections_bridge_ux_systems_logs
			select 
	--				 null,
					 1 -- custom_id 
					,'bonampakdb'
					,'bonampakdb.dbo.trafico_guia'
					,@action -- 1=c,2=r,3=u,4=d
					,'[1][' + cast("i".no_guia as char(10)) + '][' + cast("i".id_area as char(1)) + ']' + '[' + cast("i".num_guia as char(10)) + ']' -- data_crud
					,current_timestamp
					,current_timestamp
					,1
			from 
					inserted as "i"
	-- then save in statistics
	-- Run sp
end

--========================================================================================================================================= --
use macuspanadb
-- drop trigger monitor_ux_insert_update
create trigger monitor_ux_insert_update
ON macuspanadb.dbo.trafico_guia
with encryption
after insert,update
as 
begin
	-- Catch the rows for examination
	set nocount on
	
	declare  @action as int
	
	if  COLUMNS_UPDATED() <> 0 -- delete or update?
		begin 
		  if exists  (select * from deleted) -- updated cols + old rows means action=update       
		    set @action = 3  -- UPDATE
		  else 
		  	set @action = 1  -- 'INSERT' -- updated columns and nothing deleted means action=insert
		end 
	else -- delete     
		begin 
			set @action = 4	 -- 'DELETE'
		end 
	
		insert into sistemas.dbo.projections_bridge_ux_systems_logs
			select 
	--				 null,
					 2 -- custom_id 1-tbk , 2-atm , 3-tei
					,'macuspanadb'
					,'macuspanadb.dbo.trafico_guia'
					,@action -- 1=c,2=r,3=u,4=d
					,'[1][' + cast("i".no_guia as char(10)) + '][' + cast("i".id_area as char(1)) + ']' + '[' + cast("i".num_guia as char(10)) + ']' -- data_crud
					,current_timestamp
					,current_timestamp
					,1
			from 
					inserted as "i"
	-- then save in statistics
	-- Run sp
end



--========================================================================================================================================= --

use tespecializadadb
-- drop trigger monitor_ux_insert_update
create trigger monitor_ux_insert_update
ON tespecializadadb.dbo.trafico_guia
with encryption
after insert,update
as 
begin
	-- Catch the rows for examination
	set nocount on
	
	declare  @action as int
	
	if  COLUMNS_UPDATED() <> 0 -- delete or update?
		begin 
		  if exists  (select * from deleted) -- updated cols + old rows means action=update       
		    set @action = 3  -- UPDATE
		  else 
		  	set @action = 1  -- 'INSERT' -- updated columns and nothing deleted means action=insert
		end 
	else -- delete     
		begin 
			set @action = 4	 -- 'DELETE'
		end 
	
		insert into sistemas.dbo.projections_bridge_ux_systems_logs
			select 
	--				 null,
					 3 -- custom_id 1-tbk , 2-atm , 3-tei
					,'tespecializadadb'
					,'tespecializadadb.dbo.trafico_guia'
					,@action -- 1=c,2=r,3=u,4=d
					,'[1][' + cast("i".no_guia as char(10)) + '][' + cast("i".id_area as char(1)) + ']' + '[' + cast("i".num_guia as char(10)) + ']' -- data_crud
					,current_timestamp
					,current_timestamp
					,1
			from 
					inserted as "i"
	-- then save in statistics
	-- Run sp
end

-- ================================================================================================================== --
		
with operations_ind
						(  
								 company,id_area,area
	--							,id_flota,flota
								,id_tipo_operacion
								,id_fraccion
								,fraccion
								,cyear,mes
								,kms_real
								,kms_viaje
								,subtotal
								,peso
								,non_zero
						)
		as (
				select 
						 indupt.company,indupt.id_area
						 ,--indupt.area
	--				case	
	--				 	when indupt.id_tipo_operacion in 	(
	--				 											select confi.module_data_definition from sistemas.dbo.projections_configs as confi
	--				 											where confi.projections_type_configs_id = 5
	--				 										)
	--				 		then
	--				 			(
	--			 					select module_field_translation from sistemas.dbo.projections_configs as conf 
	--			 					where conf.projections_type_configs_id = 5 and conf.module_data_definition = indupt.id_tipo_operacion
	--				 			)
	--				 	else
	--				 		indupt.area collate SQL_Latin1_General_CP1_CI_AS -- as 'area'
	--				  end as 'area'
					  
					case	
					 	when indupt.id_tipo_operacion = 12
					 		then
					 			case 
					 				when indupt.area = 'GUADALAJARA'
					 					then 'LA PAZ'
					 				else
					 					indupt.area collate SQL_Latin1_General_CP1_CI_AS -- as 'area'
					 			end
					 	else
					 		indupt.area collate SQL_Latin1_General_CP1_CI_AS -- as 'area'
					  end as 'area'
					  
	--					,indupt.id_flota,indupt.flota
						,indupt.id_tipo_operacion
						,indupt.id_fraccion
	--					,indupt.fraccion
						,case
							when indupt.id_fraccion is null
								then 'VACIO'
							else
								indupt.fraccion
						end as 'fraccion'
						,year(indupt.f_despachado) as 'cyear'
						,indupt.mes
						--,tipo_de_operacion
						,case
							when indupt.trip_count = 1
								then sum(indupt.kms_real)
							when indupt.trip_count is null
								then sum(isnull(indupt.kms_real,0))
							else
								cast ((select '0') as int)
						end as 'kms_real'
	--					,sum(kms_real) as 'kms_real'
						,case
							when indupt.trip_count = 1
								then sum(indupt.kms_viaje)
							when indupt.trip_count is null
								then sum(isnull(indupt.kms_viaje,0))
							else
								cast ((select '0') as int)
						end as 'kms_viaje'
						--,sum(kms_viaje) as 'kms-viaje'
						,sum(indupt.subtotal) as 'subtotal',sum(indupt.peso) as 'peso'
						,case
							when indupt.trip_count = 1
								then count(indupt.trip_count)
							when indupt.trip_count is null
								then count(indupt.id_area)
							else
								cast ((select '0') as int)
						end as 'non_zero'
				from	
						sistemas.dbo.projections_view_full_company_dispatched_indicators as indupt
						--sistemas.dbo.view_sp_xd6z_getFullCompanyOperationsDispatch as indupt						
				group by
						 indupt.company,indupt.id_area,indupt.area
	--					,indupt.id_flota,indupt.flota
						,indupt.id_tipo_operacion
						,indupt.id_fraccion,indupt.fraccion				
						,year(indupt.f_despachado)
						,indupt.mes
						,indupt.trip_count		
			)
	select
			row_number()
		over 
			(order by id_area) as 
								id,
								company,
								id_area,
								area,
	--							id_flota,
	--							flota,
	--							id_tipo_operacion,
	--							tipo_operacion,
								id_fraccion,
								fraccion,
								cyear,
								mes,
								kms,
								subtotal,
								peso,
								non_zero
	from(
			select 			
					opsind.company
					,cast(opsind.id_area as int)  as 'id_area'
					,
	--				case	
	--				 	when opsind.id_tipo_operacion in 	(
	--				 											select confi.module_data_definition from sistemas.dbo.projections_configs as confi
	--				 											where confi.projections_type_configs_id = 5
	--				 										)
	--				 		then
	--				 			(
	--			 					select module_field_translation from sistemas.dbo.projections_configs as conf 
	--			 					where conf.projections_type_configs_id = 5 and conf.module_data_definition = opsind.id_tipo_operacion
	--				 			)
	--				 	else
					 		opsind.area collate SQL_Latin1_General_CP1_CI_AS as 'area'
					  --end as 'area'
	--				,opsind.id_flota,opsind.flota
	--				,opsind.id_tipo_operacion
	--				,isnull(tipoop.tipo_operacion,'Vacio') as 'tipo_operacion'
					,opsind.id_fraccion
					,opsind.fraccion
					,opsind.cyear
					,opsind.mes
					,case -- which field we have to show
						when opsind.id_fraccion in ( 
												select prfrt.projections_id_fraccion
												from sistemas.dbo.projections_view_company_fractions as prfrt
												where prfrt.projections_corporations_id = opsind.company and prfrt.projections_rp_fraction_id = 1) -- means granel
							then 
								(sum(opsind.kms_viaje)*2)
						when opsind.id_fraccion not in (	
												select prtrf.projections_id_fraccion
												from sistemas.dbo.projections_view_company_fractions as prtrf
												where prtrf.projections_corporations_id = opsind.company and prtrf.projections_rp_fraction_id = 1) -- means otros
							then 
								sum(opsind.kms_real)
						else
							sum(opsind.kms_real)
					end as 'kms'
					,sum(opsind.subtotal) as 'subtotal'
					,sum(opsind.peso) as 'peso'
					,sum(opsind.non_zero) as 'non_zero'
			from 
					operations_ind opsind
	--				left join 
	--							sistemas.dbo.projections_view_operations_types as tipoop 
	--							on opsind.company = tipoop.company_id and opsind.id_tipo_operacion = tipoop.id_tipo_operacion
	
			group by 
					 opsind.company
					 ,opsind.id_area
					 ,opsind.area
	-- 				 ,opsind.id_flota
	-- 				 ,opsind.flota
	-- 				 ,opsind.id_tipo_operacion
	-- 				 ,tipoop.tipo_operacion
					 ,opsind.id_fraccion
					 ,opsind.fraccion
					 ,opsind.cyear
					 ,opsind.mes
	) as result
		
		
							
							
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company    ======================================== --
-- ==================================================================================================================== --
-- Build for general core
-- projections_view_full_company_core_indicators
				
				
use sistemas
IF OBJECT_ID ('projections_view_full_company_core_small_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_core_small_indicators;
    
create view projections_view_full_company_core_small_indicators
as 
(
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,num_guia ,fecha_guia ,mes ,f_despachado ,cliente ,kms_viaje ,kms_real,kms,subtotal ,[peso-despachado] as 'peso' 
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count
			,fecha_cancelacion,projections_rp_definition,no_guia,f_despachado_m,fecha_guia_m,fecha_cancelacion_m,fecha_confirmacion_m,FlagIsDisminution,FlagIsProvision
	from 
			sistemas.dbo.projections_view_full_indicators_tbk_core_periods
	union all
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,num_guia ,fecha_guia ,mes ,f_despachado ,cliente ,kms_viaje ,kms_real ,kms ,subtotal ,[peso-despachado] as 'peso' 
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count
			,fecha_cancelacion,projections_rp_definition,no_guia,f_despachado_m,fecha_guia_m,fecha_cancelacion_m,fecha_confirmacion_m,FlagIsDisminution,FlagIsProvision
	from 
			sistemas.dbo.projections_view_full_indicators_atm_core_periods
	union all
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,num_guia,fecha_guia ,mes ,f_despachado ,cliente ,kms_viaje ,kms_real ,kms, subtotal ,[peso-despachado] as 'peso'
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count
			,fecha_cancelacion,projections_rp_definition,no_guia,f_despachado_m,fecha_guia_m,fecha_cancelacion_m,fecha_confirmacion_m,FlagIsDisminution,FlagIsProvision
	from 
			sistemas.dbo.projections_view_full_indicators_tei_core_periods
) 

				
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company    ======================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_company_discore_small_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_discore_small_indicators;
    
create view projections_view_full_company_discore_small_indicators
as 				
select 
		 "dissmiss".id_area
		,"dissmiss".id_unidad
		,"dissmiss".id_configuracionviaje
		,"dissmiss".id_tipo_operacion
		,"dissmiss".id_fraccion
		,"dissmiss".id_flota
		,"dissmiss".no_viaje
		,"dissmiss".num_guia
		,"dissmiss".fecha_guia
		,"dissmiss".mes
		,"dissmiss".f_despachado
		,"dissmiss".cliente
		,"dissmiss".kms_viaje
		,"dissmiss".kms_real
		,"dissmiss".kms*-1 as 'kms'
		,"dissmiss".subtotal*-1 as 'subtotal'
		,"dissmiss".peso*-1 as 'peso'
		,"dissmiss".configuracion_viaje
		,"dissmiss".tipo_de_operacion
		,"dissmiss".flota
		,"dissmiss".area
		,"dissmiss".fraccion
		,"dissmiss".company
		,"dissmiss".trip_count*-1 as 'trip_count'
		,"dissmiss".fecha_cancelacion
		,"dissmiss".projections_rp_definition
		,"dissmiss".no_guia
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int)  as 'f_despachado_m'
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_guia_m'
		,'' as 'fecha_cancelacion_m'
		,"dissmiss".fecha_confirmacion_m
		,"dissmiss".FlagIsDisminution
		,"dissmiss".FlagIsProvision
from 
		sistemas.dbo.projections_view_full_company_core_small_indicators as "dissmiss"
where 
		FlagIsDisminution = 1			
				
				
	

-- ====================================================================================================== --
-- ==============   Start Query About 'Despachado' Indicators with the new standart in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability
use sistemas
IF OBJECT_ID ('projections_view_full_company_core_desp_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_core_desp_indicators;
--    
--create view projections_view_full_company_core_desp_indicators
--with encryption
--as 				
--with "despachado" as (
--						select 
----								row_number() over (order by "desp".id_area) as 'id'
--								 "desp".company
--								,"desp".id_area
--								,"desp".area
--								,"desp".id_fraccion
--								,"desp".fraccion
--								,year("desp".f_despachado) as 'cyear'
--								,"desp".[mes-despacho] as 'mes'
--								,sum("desp".kms) as 'kms'
--								,sum("desp".subtotal) as 'subtotal'
--								,sum("desp".[peso-despachado]) as 'peso'
--								,sum("desp".trip_count) as 'non-zero'
--								,"desp".f_despachado_m
--								,"desp".FlagIsDisminution
--								,"desp".FlagIsProvision
--						from
--								sistemas.dbo.projections_view_full_company_core_indicators as "desp"
--						where
--								"desp".FlagIsDisminution <> 2 -- aceptado + canceladas
--						and 
--								"desp".no_guia is not null
--						and 
--								year("desp".f_despachado) in ( --- in year ?
--																 year(current_timestamp)
--																,year(dateadd(yy,-1,cast(current_timestamp as date)))
--														     )
--						group by 
----								 id
--								 "desp".company
--								,"desp".id_area
--								,"desp".area
--								,"desp".id_fraccion
--								,"desp".fraccion
--								,year("desp".f_despachado)
--								,"desp".[mes-despacho]
--								,"desp".f_despachado_m
--								,"desp".FlagIsDisminution
--								,"desp".FlagIsProvision
--			union all	
--						select 
----								row_number() over (order by "desp".id_area) as 'id'
--								 "desp".company
--								,"desp".id_area
--								,"desp".area
--								,"desp".id_fraccion
--								,"desp".fraccion
--								,year("desp".fecha_cancelacion) as 'cyear'
--								,"desp".[mes-despacho] as 'mes'
--								,sum("desp".kms) as 'kms'
--								,sum("desp".subtotal) as 'subtotal'
--								,sum("desp".[peso-despachado]) as 'peso'
--								,sum("desp".trip_count) as 'non-zero'
--								,"desp".f_despachado_m
--								,"desp".FlagIsDisminution
--								,"desp".FlagIsProvision
--						from
--								sistemas.dbo.projections_view_full_company_discore_indicators as "desp"
--						where
--								"desp".FlagIsDisminution = 1 -- aceptado + canceladas
--						and 
--								"desp".no_guia is not null
--						and 
--								year("desp".fecha_cancelacion) in ( --- in year ?
--																 year(current_timestamp)
--																,year(dateadd(yy,-1,cast(current_timestamp as date)))
--														     )
--						group by 
----								 id
--								 "desp".company
--								,"desp".id_area
--								,"desp".area
--								,"desp".id_fraccion
--								,"desp".fraccion
--								,year("desp".fecha_cancelacion)
--								,"desp".[mes-despacho]
--								,"desp".f_despachado_m
--								,"desp".FlagIsDisminution
--								,"desp".FlagIsProvision
--)
--select 
--	 row_number() over (order by id_area) as 'id'
--	,company
--	,id_area
--	,area
--	,id_fraccion
--	,fraccion
--	,cyear
--	,mes
--	,kms
--	,subtotal
--	,peso
--	,[non-zero]
--	,f_despachado_m
--	,FlagIsDisminution
--	,FlagIsProvision
--from "despachado" -- left joint to areas ??
--















		
with "despachado" as (	
-- ACEPTADO FlagIsProvision => 0 + PROVISION FlagIsProvision => 1
select 
		sum(subtotal) as 'subtotal', sum(kms_viaje*2) as 'kms_viaje',sum(kms_real) as 'kms_real',sum(kms) as 'kms' , sum(peso) as 'peso',sum(trip_count) as 'viajes'
		,FlagIsDisminution , FlagIsProvision ,area,fraccion,projections_rp_definition 
from 
		sistemas.dbo.projections_view_full_company_core_small_indicators
where 
		f_despachado_m in (201811) 
--where fecha_guia_m in (201810) 
	and 
		FlagIsDisminution <> 2 -- aceptado + canceladas
	and 
		area = 'GUADALAJARA' 
	and 
		no_guia is not null
group by 
		 area
		,FlagIsDisminution 
		,FlagIsProvision
		,fraccion
		,projections_rp_definition
union all
-- CANCELADOMesAnterior/Disminucion [DESPACHADO]
select 
		sum(subtotal) as 'subtotal', sum(kms_viaje*2) as 'kms_viaje',sum(kms_real) as 'kms_real',sum(kms) as 'kms' , sum(peso) as 'peso',sum(trip_count) as 'viajes'
		,FlagIsDisminution , FlagIsProvision ,area ,fraccion ,projections_rp_definition 
from 
		sistemas.dbo.projections_view_full_company_discore_small_indicators
where 
		f_despachado_m in (201811) 
--where fecha_guia_m in (201810) 
--	and 
--		FlagIsDisminution <> 2 -- aceptado + canceladas
	and 
		area = 'GUADALAJARA' 
	and 
		no_guia is not null
group by 
		 area
		,FlagIsDisminution 
		,FlagIsProvision
		,fraccion
		,projections_rp_definition
)
select 
		 subtotal
		,kms_viaje
		,kms_real
		,kms
		,peso
		,Viajes
		,area
		,fraccion
		,projections_rp_definition 
		,FlagIsDisminution 
		,FlagIsProvision
from
		"despachado"
--group by 
--		area
--		,fraccion
--		,projections_rp_definition
--		,FlagIsDisminution 
--		,FlagIsProvision
	 

		
		
		
	

select cast(substring( convert(nvarchar(MAX), dateadd(mm,-1,current_timestamp), 112) , 1, 6 ) as int)


select 
*
from 
		sistemas.dbo.projections_view_full_company_dispatched_indicators --where no_viaje = 102406 and id_area = 2
where 
		year(f_despachado) in (2018)  and month(f_despachado) = 10

select 
		* 
from 
		sistemas.dbo.projections_view_full_company_core_indicators -- where no_viaje = 102406 and id_area = 2
where f_despachado_m = 201810


select 
		* 
from 
		sistemas.dbo.projections_view_full_indicators_tbk_core_periods --where no_viaje = 102406 and id_area = 2
where f_despachado_m = 201810




with "aceptado" as (	
-- ACEPTADO FlagIsProvision => 0 + PROVISION FlagIsProvision => 1
select sum(subtotal) as 'subtotal',sum(kms) as 'kms' , sum(peso) as 'peso',sum(trip_count) as 'Viajes'
, FlagIsDisminution , FlagIsProvision
,area
,fraccion
,projections_rp_definition 
from sistemas.dbo.projections_view_full_company_core_indicators
--where f_despachado_m in (201810) 
where fecha_guia_m in (201810) 
	and 
		FlagIsDisminution <> 2 -- aceptado					
	and area = 'GUADALAJARA' 
	and no_guia is not null
group by 
	 area
	 ,FlagIsDisminution , FlagIsProvision
	 ,fraccion
	 ,projections_rp_definition
union all
-- disminucion [DESPACHADO]
select (sum(subtotal) * -1) as 'subtotal' , sum(kms)*-1 as 'kms' , sum(peso)*-1 as 'peso',sum(trip_count)*-1 as 'Viajes'
--, FlagIsDisminution , FlagIsProvision
,area
,fraccion
,projections_rp_definition 
from sistemas.dbo.projections_view_full_company_core_indicators
-- where f_despachado_m in (201809) 
where fecha_cancelacion_m in (201810) 
	and 
		FlagIsDisminution = 1 -- keeps in backwards-month and dissmiss in current
	and 
		area = 'GUADALAJARA'
	and 
		no_guia is not null
group by 
	  FlagIsDisminution , FlagIsProvision, 
	  area 
	 ,fraccion
	 ,projections_rp_definition
)
select 
		 sum(subtotal) as 'subtotal'
		,sum(kms) as 'kms'
		,sum(peso) as 'peso'
		,sum(Viajes) as 'viajes'
		,area
		,fraccion
		,projections_rp_definition 
from
		"aceptado"
group by 
		area
		,fraccion
		,projections_rp_definition
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	

		

-- mantiene en mes , disminuye siguiente	

select * from sistemas.dbo.projections_view_full_indicators_tbk_periods_tests
where f_despachado_m in (201809,201810,201811)
and FlagIsProvision = 1  



select * from sistemas.dbo.projections_view_full_indicators_tbk_periods_tests
where no_viaje = '43841' and id_area = 1 and company = 1

select * from integraapp.dbo.zpoling where CPorte in ('OO-053705')


select * from "bonampakdb"."dbo"."v_ingresos2013"
