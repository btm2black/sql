-- ============================================================================================================== --
-- Indicadores Operaciones 
-- ============================================================================================================== --


-- ============================================================================================================== --
-- Indicador de Rendimiento 
-- ============================================================================================================== --
select * from sistemas.dbo.projections_periods
--select * from sistemas.dbo.projections_view_gst_areas

--select * from sistemas.dbo.projections_
--- Esta consulta nos permite obtener los datos generales de los viajes realizados. 

select 
	tg.no_viaje as "VIAJE",
	pp.nombre as "OPERADOR",
	tg.id_unidad as "TRACTO",
	tg.id_tipo_operacion as "CONFIG",		--- Se debe obtener la descripcion de la tabla "desp_tipooperacion"
	tg.fecha_guia as "FECHA",
	tg.id_origen as "ORIGEN",		--- Se debe obtener la descripcion de la plaza de la tabla "trafico_plaza"
	tg.id_destino as "DESTINO",		--- Se debe obtener la descripcion de la plaza de la tabla "trafico_plaza"
	mu.modelo as "MODELO",
	tv.kms_real as "KMS",
	tv.lts_empresa as "DIESEL"	
from 
	gstdb.dbo.trafico_guia tg
inner join
	gstdb.dbo.trafico_viaje tv on tg.no_viaje = tv.no_viaje and tg.id_area = tv.id_area
inner join 
	gstdb.dbo.mtto_unidades mu on mu.id_unidad = tg.id_unidad
inner join
	gstdb.dbo.personal_personal pp on tg.id_personal = pp.id_personal
where		------- En este bloque se define de que area se obtiene la informacion, asi como el rango de fecha.
--	tg.id_area = 2
--and 
	tg.id_origen = tv.id_origen
and
	tg.id_destino = tv.id_destino
and
	tg.fecha_guia BETWEEN '2019-01-01' and '2019-01-31'
and 
	tg.status_guia <> 'B'
and
	tv.no_liquidacion is not null
and 
	tv.no_viaje = '45984'
-- ============================================================================================================== --
-- Build of Ind 
-- ============================================================================================================== --
-- ===================================================================================================================== --		
--  This table are the full core for all gst and replace old  
-- ===================================================================================================================== --

-- select * from sistemas.dbo.rend_view_full_gst_core_indicators where periodo = 201901 and id_area = 1 and tracto = 'TT1369'
 select fecha_liquidacion,no_viaje,kms_real,status_guia,* from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje in (54360,56282,56284,56286,56288,57441)

 121105,126638
 
 select * from sistemas.dbo.rend_view_full_gst_core_indicators where viaje in (121105,126638) area = 'ORIZABA' and year(fecha) = 2019 and month(fecha) = 08
 
use sistemas;
IF OBJECT_ID ('rend_view_full_gst_core_indicators', 'V') IS NOT NULL
    DROP VIEW rend_view_full_gst_core_indicators;
-- now build the view
alter view rend_view_full_gst_core_indicators
with encryption
as -- fraccion
	select
			 row_number() over(order by "rendimiento".no_viaje) as 'id'
			,"rendimiento".no_viaje as 'viaje'
			,"rendimiento".id_area
			,ltrim(rtrim(isnull("rendimiento".ciudad,"rendimiento".area))) as 'area'
			,"rendimiento".operador 
			,"rendimiento".cliente -- NEW ADD  Change means Cliente Paga
			,ltrim(rtrim("rendimiento".id_unidad)) as 'tracto'
			,"rendimiento".id_tipo_operacion
			,case "asignacion".id_seguimiento
				when 1 then 'Cargado'
				when 2 then 'Vacio'
			 else 
			 	''
			 end as 'TipoViaje'
			,ltrim(rtrim(replace("rendimiento".tipo_de_operacion,'FLETES' , ''))) as 'tipoOperacion'
			,cast("rendimiento".fecha_liquidacion as date) as 'fecha' -- Change before are fecha_guia
			,ltrim(rtrim("rendimiento".origen)) as 'origen' -- 
			,ltrim(rtrim("rendimiento".destino)) as 'destino' -- plaza
			,ltrim(rtrim("rendimiento".origen)) + ' - ' + ltrim(rtrim("rendimiento".destino)) as 'route'
			,ltrim(rtrim("rendimiento".modelo)) as 'modelo'
			,cast("rendimiento".kms_real as decimal(18,2)) as 'kms'
			,cast(("rendimiento".lts_empresa + "rendimiento".lts_viaje) as decimal(18,2)) as 'diesel'
			,case 
				when 
					("rendimiento".lts_empresa + "rendimiento".lts_viaje) = 0.00
				then 
					0
			 else 
					cast( ("rendimiento".kms_real / ("rendimiento".lts_empresa + "rendimiento".lts_viaje) ) as decimal(18,2))
			 	end 
			 as 'rendimiento'
			,"rendimiento".rendimiento_reseteo  -- NEW ADD  Change
			,"rendimiento".rendimiento_real  -- NEW ADD  Change
			,"rendimiento".trip_count as 'viajes'
			,"rendimiento".fecha_guia_m as 'periodo'
			,"rendimiento".mes
			,year("rendimiento".fecha_guia) as 'cyear'
			,'' as 'monto_comb' --"combustible".monto_comb --subtotal, costo diesel 
			,'' 'monto_precio' --"combustible".monto_precio 
			,'' as 'cantidad_comb' --"combustible".cantidad_comb
			,'' as 'observaciones' -- "combustible".observaciones
	from 
			sistemas.dbo.projections_view_full_gst_core_indicators as "rendimiento"
		inner join 
			gstdb.dbo.desp_asignacion as "asignacion"
		on 
			"rendimiento".id_area = "asignacion".id_area 
		and
			"rendimiento".no_viaje = "asignacion".no_viaje		
--		inner join 
--			gstdb.dbo.trafico_combustible as "combustible"
--		on 
--			"rendimiento".id_area = "combustible".id_area 
--		and 
--			"asignacion".no_viaje = "combustible".no_viaje
--		and 
--			"asignacion".id_asignacion = "combustible".id_asignacion
	where
			"rendimiento".no_liquidacion is not null
		and 
			"rendimiento".trip_count = 1
--		and 
--			"rendimiento".tipo_doc = 2 
		and
			(
				"rendimiento".status_guia <> 'B'
			or 
				"rendimiento".status_guia is null
			)
--		and 
--			"rendimiento".no_viaje = 19476
--		and ltrim(rtrim("rendimiento".ciudad)) = 'Hermosillo' 
--		and cast("rendimiento".fecha_liquidacion as date) between '2019-01-01' and '2019-06-30'
		and 
			"rendimiento".no_viaje in (19640,19476)
			
		

		
	select id_area,area,ciudad from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 53100	
		
	select no_viaje,num_guia,status_asignacion,* from sistemas.dbo.projections_view_full_gst_core_indicators where num_guia is null and no_liquidacion is not null 
	and no_viaje = 19640	
	
	select * from sistemas.dbo.rend_view_full_gst_core_indicators where viaje = 19476 and status_guia <> 'B'

	select * from sistemas.dbo.rend_view_full_gst_core_indicators where viaje = 19640

	select status_guia,tipo_doc,* from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 19640--19476
		
--  ============================================================================================================== --						


		
		--TODO for indicator

 select
        "viaje".no_viaje
	   ,"viaje".id_area
	   ,"asig".id_asignacion
	   ,"combustible".monto_comb
	   ,"combustible".monto_precio
  from
       gstdb.dbo.trafico_viaje as "viaje"
  inner join
       gstdb.dbo.desp_asignacion as "asig"
  on
       "viaje".id_area = "asig".id_area
  and
       "viaje".no_viaje = "asig".no_viaje
  inner join
       gstdb.dbo.trafico_combustible as "combustible"                                                                                                              
  on
       "viaje".id_area = "combustible".id_area
  and
       "asig".id_asignacion = "combustible".id_asignacion
  where
       "viaje".no_viaje = 51442;

		
cliente que paga = id_cliente en trafico guia 

select top 100 id_cliente,cliente,rendimiento_reseteo,* from sistemas.dbo.projections_view_full_gst_core_indicators


select top 100 rendimiento_real,rendimiento_reseteo,rendimiento_viaje from gstdb.dbo.trafico_viaje


--rendimiento_real --> rendimiento_reseteo ,rendimiento_real [trafico_viaje]


trafico_viaje -> desp_asignacion -> trafico_combustible
no_viaje,id_area  -> id_asignacion,id_area,no_viaje ->  id_area,id_asignacion


select top 100 id from gstdb.dbo.trafico_viaje

select id_asignacion from gstdb.dbo.trafico_viaje where no_viaje = 51442 and id_area = 1		





--get diesel costs
 Search by id_asignacion

 
 
 
 select	
	monto_comb, --subtotal, costo diesel 
	monto_precio, 
	cantidad_comb,
	no_viaje,
	id_asignacion,
	*
from
gstdb.dbo.trafico_combustible
where
no_consecutivo = '049002' -- 
		
-- ============================================================================================================== --	
-- Indicador de Disponibilidad
-- ============================================================================================================== --

--- Consulta de disponibilidad

Select 
mu.id_unidad as "UNIDAD",
pp.nombre as "OPERADOR",
tk.id_rem1 as "REMOLQUE",
mu.id_status as "ESTATUS",				---- Se obtiene la descripcion de la tabla "desp_status"
mu.id_tipo_operacion as "SEGMENTO"		---- Se obtine la descripcion de la tabla "desp_tipooperacion"
From 
gstdb.dbo.mtto_unidades mu
inner join
gstdb.dbo.trafico_kit_unidad tk on mu.id_unidad = tk.unidad_motriz
inner join 
gstdb.dbo.personal_personal pp on mu.id_operador = pp.id_personal
where 
	mu.id_area= 2

-- ============================================================================================================== --
-- Build of 
-- ============================================================================================================== --

use sistemas -- INCOMPLETE
-- select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Orizaba' and segmento = 'GRANEL'
IF OBJECT_ID ('disponibilidad_view_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_unidades_gst_indicators;
-- now build the view
alter view disponibilidad_view_unidades_gst_indicators
with encryption
as
-- with "unit" as (
select 
		 "unidades".id_unidad as 'unidad'
		,"unidades".id_status as 'idestatus'
		,"status".nombre as 'estatus'
		,"status".tipo_status
--		,"unidades".id_tipo_operacion as 'segmento'
		,"personal".nombre as 'operador'
		,"kit".id_rem1 as 'remolque'
		,"area".ciudad as 'area'
		,ltrim(rtrim(replace("operacion".tipo_operacion,'FLETES' , ''))) as 'segmento'
from 
		gstdb.dbo.mtto_unidades	as "unidades"
inner join 
		gstdb.dbo.personal_personal as "personal"
	on 
		"unidades".id_operador = "personal".id_personal
inner join 
		gstdb.dbo.general_area as "area"
	on 
		"unidades".id_area = "area".id_area
inner join 
		gstdb.dbo.desp_tipooperacion as "operacion"
	on 
		"unidades".id_tipo_operacion = "operacion".id_tipo_operacion
inner join 
		gstdb.dbo.desp_status as "status"
	on 
		"unidades".id_status = "status".id_status
left join 	
		gstdb.dbo.trafico_kit_unidad as "kit"
	on 
		"unidades".id_unidad = "kit".unidad_motriz
where 
		"unidades".id_tipo_unidad = 1
--)



-- select * from "unit" where "unit".area = 'Orizaba'
-- =================================================================================================== --	
-- =============  Build of store table for Description and Compromise Date
-- =================================================================================================== --		

-- select * from 
-- truncate
-- table 
-- select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators

use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table disponibilidad_tbl_unidades_gst_indicators(
		 id					int 			identity(1,1)
		,unidad				char(6)			null
		,id_status			int				null
		,user_id			int				null
		,description		text 			null
		,compromise			date			null
		,created			datetime		null
		,modified			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off
-- go
-- test


--insert into sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
--	select 
--			'TT1175',3,'en XXXcorralon',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1

-- ======================================================================================================== --
-- build of disponibilidad_view_join_unidades_gst_indicators 	
-- build an historical as view	
-- ======================================================================================================== --	
use sistemas
--select * from sistemas.dbo.disponibilidad_view_id_unidades_gst_indicators
-- join unidades description
IF OBJECT_ID ('disponibilidad_view_id_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_id_unidades_gst_indicators;
-- now build the view
create view disponibilidad_view_id_unidades_gst_indicators
with encryption
as
	select 
		 max("compromise".id) as 'id'
		,"compromise".unidad
	from 
		sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "compromise"
	group by 
		"compromise".unidad
		
-- ======================================================================================================== --	

use sistemas
-- check unidades con viaje 
-- select * from sistemas.dbo.disponibilidad_view_viajes_gst_indicators
IF OBJECT_ID ('disponibilidad_view_viajes_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_viajes_gst_indicators;
-- now build the view
create view disponibilidad_view_viajes_gst_indicators
with encryption
as
select 
		 "trajico".id_unidad 
		,"trajico".viajeactual as 'status_viaje'
		,'unidad en viaje' as 'desc_viaje'
from 
		gstdb.dbo.trafico_viaje  as "trajico"
where 
		viajeactual = 'S'
group by 
		id_unidad,viajeactual

-- ======================================================================================================== --

use sistemas
-- check unidades con viaje 
select * from sistemas.dbo.disponibilidad_view_taller_gst_indicators
IF OBJECT_ID ('disponibilidad_view_taller_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_taller_gst_indicators;
-- now build the view
--create
	alter view disponibilidad_view_taller_gst_indicators
with encryption
as
select 
		 "ord".id_area
		,"ord".id_orden
		,"unit".id_unidad
		,"ord".estado as 'status_taller'
		,'Unidad en Mantenimiento' as 'desc_taller'
		,"ord".fecha_inicio
		,"ord".fecha_prometida
from 
		gstdb.dbo.mtto_orden as "ord"
inner join 
		gstdb.dbo.mtto_unidades as "unit"
	on 
		"unit".id_tipo_unidad = 1 
	and
		"unit".id_unidad = "ord".id_unidad
where 
		"ord".estado = 'A'
		
		
-- ======================================================================================================== --		
use sistemas
-- check unidades con viaje 
IF OBJECT_ID ('disponibilidad_view_status_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_status_gst_indicators;
-- now build the view
create view disponibilidad_view_status_gst_indicators
with encryption
as
select
		"status".id_status,"status".nombre
from 
		gstdb.dbo.desp_status as "status"

-- ======================================================================================================== --		
-- Principal view
-- ======================================================================================================== --
-- NOTE ADD join to test table as mtto_unidades id_status and estatus fields 
-- ======================================================================================================== --
select * from gstdb.dbo.desp_flotas
-- select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Orizaba' and segmento = 'GRANEL'
select * from gstdb.dbo.mtto_unidades where id_tipo_unidad in (1,13)

use sistemas
-- select * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where id_status is null or id_status = ''
IF OBJECT_ID ('disponibilidad_view_rpt_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_rpt_unidades_gst_indicators;
-- now build the view
alter view disponibilidad_view_rpt_unidades_gst_indicators
with encryption
as		
select 
		 "unidades".id_unidad as 'unidad'
		,"flotas".id_flota
		,"flotas".nombre as 'flota'
		,"unidades".id_status as 'xid_status'
		,"status".nombre as 'xestatus'
		,"status".tipo_status
		/*-- STALL SECTION */
		,"stall_unidades".id_status as 'id_status'
		,"stall_status".nombre as 'estatus'
		/*-- STALL SECTION */
		,isnull("personal".nombre,'Sin Operador') as 'operador'
		,"kit".id_rem1 as 'remolque'
		,"area".id_area
		,"area".ciudad as 'area'
		,"operacion".id_tipo_operacion
		,ltrim(rtrim(replace("operacion".tipo_operacion,'FLETES' , ''))) as 'segmento'
		,"tbl_unit".description
		,"tbl_unit".compromise
		,"isviaje".status_viaje
		,"isviaje".desc_viaje
		,"istaller".status_taller
		,"istaller".desc_taller
		,case
			when 
					("isviaje".status_viaje = 'S' or "istaller".status_taller = 'A')
				then 
					0 
			else 
				1 
		end as 'iseditable'	
		,"istaller".id_area as 'id_area_taller'
		,"area_taller".ciudad as 'area_taller'
		,"istaller".id_orden
		,"istaller".fecha_inicio
		,"istaller".fecha_prometida
from 
--		gstdb.dbo.mtto_unidades	as "unidades"
		gstdb.dbo.mtto_unidades	as "unidades"

inner join 
		gstdb.dbo.general_area as "area"
	on 
		"unidades".id_area = "area".id_area
inner join 
		gstdb.dbo.desp_tipooperacion as "operacion"
	on 
		"unidades".id_tipo_operacion = "operacion".id_tipo_operacion
inner join 
		gstdb.dbo.desp_status as "status"
	on 
		"unidades".id_status = "status".id_status
/*-- STALL SECTION */
left join 
		pruebas_tbk.dbo.mtto_unidades	as "stall_unidades"
	on 
		"unidades".id_unidad = "stall_unidades".id_unidad and "unidades".id_area = "stall_unidades".id_area
left join  
		pruebas_tbk.dbo.desp_status as "stall_status"
	on 
		"stall_unidades".id_status = "stall_status".id_status
/*-- STALL SECTION */
left join 
		gstdb.dbo.desp_flotas as "flotas"
	on 
		"unidades".id_flota = "flotas".id_flota
left join 
		gstdb.dbo.personal_personal as "personal"
	on 
		"unidades".id_operador = "personal".id_personal
left join 	
		gstdb.dbo.trafico_kit_unidad as "kit"
	on 
		"unidades".id_unidad = "kit".unidad_motriz
left join 
		sistemas.dbo.disponibilidad_view_viajes_gst_indicators as "isviaje"
	on 
		"isviaje".id_unidad = "unidades".id_unidad
left join 
		sistemas.dbo.disponibilidad_view_taller_gst_indicators as "istaller"
	on 
		"istaller".id_unidad = "unidades".id_unidad
left join 
		gstdb.dbo.general_area as "area_taller"
	on 
		"istaller".id_area = "area_taller".id_area
left join  
		sistemas.dbo.disponibilidad_view_id_unidades_gst_indicators as "id_unit"
	on 
		"id_unit".unidad = "unidades".id_unidad
left join 
		sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "tbl_unit"
	on 
		"tbl_unit".id = "id_unit".id
where 
		"unidades".id_tipo_unidad in (1,13)
--	and 
--		"unidades".id_unidad = 'TT1175'
--		
		
-- ======================================================================================================== --		
-- Principal view group 
-- ======================================================================================================== --	
use sistemas
-- select * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators
IF OBJECT_ID ('disponibilidad_view_rpt_group_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_rpt_group_gst_indicators;
-- now build the view
alter view disponibilidad_view_rpt_group_gst_indicators
with encryption
as	
select 
		 row_number() over(order by "dispgrouping".id_area) as 'id'
	 	,count("dispgrouping".unidad) as 'unidades'
		,"dispgrouping".id_status
		,"dispgrouping".estatus
		,"dispgrouping".id_area
		,"dispgrouping".area
		,"dispgrouping".id_flota
from 
		sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators as "dispgrouping"
group by 
		 "dispgrouping".id_status
		,"dispgrouping".estatus
		,"dispgrouping".id_area
		,"dispgrouping".area
		,"dispgrouping".id_flota

	
	
--add Valida si la unidad tiene orden de trabajo abierta

Select
id_area,
id_orden,
id_unidad,
estado,
fecha_inicio,
fecha_prometida
from
gstdb.dbo.mtto_orden
where
estado = 'A'
--and id_unidad =''

-- ======================================================================================================== --		
-- trigger gstdb_pba 
-- ======================================================================================================== --	
select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Orizaba' and segmento = 'GRANEL'

select * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where id_area = 1 and id_tipo_operacion = 1
	
select * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators where id_area = 1
--========================================================================================================================================= --
-- 											Triggers for projections																		--
--========================================================================================================================================= --
use sistemas
-- ======================================================================================================== --
-- NOTE Pointing to test table repoint to production when needed , this do the magic
-- ======================================================================================================== --

-- drop trigger monitor_ux_insert
alter trigger disponibilidad_update_unidad_status
ON sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
with encryption
after insert
as 
begin
-- Catch the rows for examination
	set nocount on
	-- then save in statistics
--		update gstdb.dbo.mtto_unidades
		update pruebas_tbk.dbo.mtto_unidades
			set id_status = "i".id_status 
		from 
--				gstdb.dbo.mtto_unidades as "mtto"
				pruebas_tbk.dbo.mtto_unidades as "mtto"
		inner join		
				inserted as "i"
			on
				"mtto".id_unidad = "i".unidad
		where 
				"mtto".id_unidad = "i".unidad
	-- Run sp
end

--		moreno tigers




alter trigger [gstdb].[dbo].[hesatec_unidad_en_taller] 
	on [gstdb].[dbo].[mtto_orden] for insert
as
	declare @id_unidad  varchar(10)
		
	select  @id_unidad = id_unidad from inserted
	-- Actualiza el estatus de la unidad a "En Taller"
	update 
			gstdb.dbo.mtto_unidades 
		set 
			id_status=4 
		where 
			id_unidad=@id_unidad
--insert first 		
	update
			pruebas_tbk.dbo.mtto_unidades 
		set 
			id_status=4 
		where 
			id_unidad=@id_unidad

--select id_unidad from gstdb.dbo.mtto_unidades group by id_unidad



use gstdb

alter trigger [dbo].[hesatec_unidad_disponible] 
	on [dbo].[mtto_orden] for update
as
	declare @id_unidad varchar(10)
	declare @estado char(1)

	select @id_unidad = id_unidad, @estado=estado  from inserted
	
	if (@estado='L' or @estado='N')
		-- Actualiza el estatus de la unidad a "Disponible"
		update gstdb.dbo.mtto_unidades set id_status=6 where id_unidad=@id_unidad
		update pruebas_tbk.dbo.mtto_unidades set id_status=6 where id_unidad= @id_unidad	

	
--		select id_status, *from pruebas_tbk.dbo.mtto_unidades
-- select *from pruebas_tbk.dbo.mtto_unidades
-- select *from pruebas_tbk.dbo.desp_status




-- truncate table sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
--
--select id_status,id_unidad from gstdb_pba.dbo.mtto_unidades where tipo_unidad = 1 and id_unidad = 'TT1210'
--
--select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators where unidad = 'TT1210'
--
--select id_status,id_unidad from gstdb.dbo.mtto_unidades where tipo_unidad = 1 and id_unidad = 'TT1210'	
--
--
--select * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where compromise is not null
-- ======================================================================================================== --		
-- -- Detail 
-- ======================================================================================================== --		
use sistemas
-- select * from sistemas.dbo.disponibilidad_view_historical_gst_indicators
IF OBJECT_ID ('disponibilidad_view_historical_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_historical_gst_indicators;
-- now build the view
create view disponibilidad_view_historical_gst_indicators
with encryption
as	
	select 
			 row_number() over(order by "historical".unidad) as 'id'
			,"historical".unidad 
			,"status".nombre as 'estatus'
			,"historical".description as 'descripcion'
			,"historical".compromise as 'compromiso'
			,convert(nvarchar(MAX), "historical".created, 23) as 'creacion'
	from 
			sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "historical"
	inner join 
		gstdb.dbo.desp_status as "status"
	on 
		"historical".id_status = "status".id_status
	where 
		"historical".status = 1
		
			
			
-- select * from gstdb.dbo.desp_flotas
-- 
   
-- ======================================================================================================== --		
-- Unidades Sin Documentacion 
-- ======================================================================================================== --	
use sistemas
    
IF OBJECT_ID ('georeference_view_positions_hist_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_hist_gst_indicators;
-- now build the view
create view georeference_view_positions_hist_gst_indicators
with encryption
as	
with "position" as (
select 
		 "area".id_area as 'id_area'
		,"area".ciudad as 'Area'
		,"mu".id_unidad as "Unidad"
		,"da".id_asignacion as 'Asignacion'
		,"da".status_asignacion
		,"da".seguimiento_actual
		,"da".fecha_ingreso as "FechaAsignacion"
		,"tv".no_viaje as 'Viaje'
		,(	select 
				 count("guia".num_guia)
			from 
					gstdb.dbo.trafico_viaje  as "viaje"
			left join 
					gstdb.dbo.trafico_guia  as "guia"
				on 
					"viaje".no_viaje = "guia".no_viaje and "viaje".id_area = "guia".id_area
			where 
				"viaje".no_viaje = "tv".no_viaje
		 ) as 'Cporte'
		,(
			select
					',' + "guiax".num_guia
			from 
					gstdb.dbo.trafico_guia as "guiax"
			where 
					"guiax".no_viaje = "tv".no_viaje and "guiax".id_area = "tv".id_area
			for xml path('')
		 ) as 'Guias'
		,"tv".f_despachado as "Despachado"
		,"pp".nombre as "Operador"
		,"dp".poslat as 'Latitud'
		,"dp".poslon as 'Longitud'
		,"dp".posdate as 'FechaPosition'
from
		gstdb.dbo.mtto_unidades as "mu"
inner join 
		gstdb.dbo.general_area as "area"
	on 
		"area".id_area = "mu".id_area
left join
		gstdb.dbo.desp_posicion_unidad as "dp" on "dp".id_unidad = "mu".id_unidad and "dp".id_area = "mu".id_area
	and 
		dp.posdate = ( select max("pst".posdate) from gstdb.dbo.desp_posicion_unidad as "pst" where "pst".id_unidad = dp.id_unidad and "pst".id_area = dp.id_area )
left join
		gstdb.dbo.desp_asignacion as "da" on "da".id_unidad = "mu".id_unidad and "da".id_area = "mu".id_area 
	and 
		"da".no_viaje > 0 and "da".id_seguimiento = 1 -- and da.seguimiento_actual = 1
--		"da".no_viaje = ( select max("trip".no_viaje) from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = "mu".id_unidad and "trip".id_area = "mu".id_area )
--	and 
--		"da".no_viaje > 0
	and 
		da.id_asignacion = ( select max("asg".id_asignacion) from gstdb.dbo.desp_asignacion as "asg" where "asg".id_unidad = dp.id_unidad and "asg".id_area = dp.id_area )
left join
		gstdb.dbo.trafico_viaje as "tv" on "tv".id_unidad = "mu".id_unidad and "tv".id_area = "mu".id_area -- and tv.viajeactual = 'S'
left join
		gstdb.dbo.personal_personal as "pp" on "pp".id_personal = "tv".id_personal
where
		"mu".id_tipo_unidad = 1
--	and 
--		da.status_asignacion not in (3,4) -- 1 asignaado[] , 2 transito -- 3 viaje finalizaso, 4 a.cancelado -- null [en movimiento sin viaje en lis] 
--	and 
--		dp.posdate = ( select max("pst".posdate) from gstdb.dbo.desp_posicion_unidad as "pst" where "pst".id_unidad = dp.id_unidad and "pst".id_area = dp.id_area ) 
--	and 
--		da.id_asignacion = ( select max("asg".id_asignacion) from gstdb.dbo.desp_asignacion as "asg" where "asg".id_unidad = dp.id_unidad and "asg".id_area = dp.id_area )
	and 
		tv.no_viaje = ( select max("trip".no_viaje) from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = "mu".id_unidad and "trip".id_area = "mu".id_area )
)
select 
		  row_number() over(order by "position".unidad) as 'id'
		 ,case 
			"position".status_asignacion
				when 1 then 'asignado'
				when 2 then 'transito'
				when 3 then 'finalizado'
				when 4 then 'cancelado'
			end as 'AsignacionDesc'
		 ,case
			"position".seguimiento_actual
				when 0 then 'sin viaje asignado'
				when 1 then 'viaje asignado'
			end as 'Seguimiento'
		 ,case 
		 -- ==================================================================================== --
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte = 0
		 	then 
		 		'Despachado sin Documentacion' -- 'Unidad sin Carta Porte' -- Unidad sin asignacion , sin viaje actual
		 	when 
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then 
		 		'Despachado sin Documentacion' --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when 
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then 
		 		'Despachado sin Documentacion' --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when 
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then 
		 		'Despachado con Documentos' --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when 
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then 
		 		'Despachado con Documentos' --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte > 0
		 	then 
		 		'Sin viaje actual'
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 1
		 	then 
		 		'Error en Sistema'
		 	when 
		 		"position".FechaPosition is null
		 	then 
		 		'Unidad sin posicion'
		 	when 
		 			"position".status_asignacion is null and "position".seguimiento_actual is null
		 		and 
		 			"position".Latitud between "localization".min_latitud and "localization".max_latitud
		 		and 
		 			"position".Longitud between "localization".min_longitud and "localization".max_longitud
		 	then
		 		'Unidad en Base'
		 end as 
		 		'StatusDescription'
		 ,case 
		 -- ==================================================================================== --
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte = 0
		 	then 
		 		0 -- 'Unidad sin Carta Porte' -- Unidad sin asignacion , sin viaje actual
		 	when 
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then 
		 		0 --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when 
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then 
		 		0 --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when 
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then 
		 		1 --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when 
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then 
		 		1 --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte > 0
		 	then 
		 		2 --'Sin viaje actual'
		 	when 
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 1
		 	then 
		 		3 --'Error en Sistema'
		 	when 
		 		"position".FechaPosition is null
		 	then 
		 		4 --'Unidad sin posicion'
		 	when 
		 			"position".status_asignacion is null and "position".seguimiento_actual is null
		 		and 
		 			"position".Latitud between "localization".min_latitud and "localization".max_latitud
		 		and 
		 			"position".Longitud between "localization".min_longitud and "localization".max_longitud
		 	then
		 		5 --'Unidad en Base'
		 end as 
		 		'statdesc'		 
		 ,case 
		 	when 
		 		(
			 		"position".Latitud between "localization".min_latitud and "localization".max_latitud 
			 	and
			 		"position".Longitud between "localization".min_longitud and "localization".max_longitud
			 	)
			 then 
			 	'inside base'
			 else 
			 	'outside base'
		 end as "enBase"
		 ,case 
		 	when 
		 		(
			 		"position".Latitud between "localization".min_latitud and "localization".max_latitud 
			 	and
			 		"position".Longitud between "localization".min_longitud and "localization".max_longitud
			 	)
			 then 
			 	1 -- true is in base 
			 else 
			 	0 -- is not in base
		 end as "inBase"
		,case 
			when 
				(
					"position".Asignacion is not null
				and
					"position".Viaje is null
				)
			then 
				'Viaje sin despacho'
			when 
				(
					"position".Asignacion is null
				and
					"position".Viaje is null
				)			
			then
				'En Movimiento sin despacho LIS'
		 else
		 		'En regla'
		end as 'DescriptionViaje'
		,case 
			when 
				(
					"position".Asignacion is not null
				and
					"position".Viaje is null
				)
			then 
				1 --'Viaje sin despacho'
			when 
				(
					"position".Asignacion is null
				and
					"position".Viaje is null
				)			
			then
				2 -- 'En Movimiento sin despacho LIS'
		 else
		 		0 -- 'En regla'
		end as 'traceroute'
		,"position".id_area
		,"position".Area
		,"position".Unidad
		,"position".Asignacion
		,"position".status_asignacion
		,"position".seguimiento_actual
		,"position".FechaAsignacion
		,"position".Viaje
		,"position".Cporte
		,substring("position".Guias,2,len("position".Guias)) as 'Guias'
		,"position".Despachado
		,"position".Operador
		,"position".Latitud
		,"position".Longitud
		,"position".FechaPosition
		,"localization".base
		,"localization".max_latitud
		,"localization".min_latitud
		,"localization".max_longitud
		,"localization".min_longitud
from 
		"position" --as "position"
cross join 
		sistemas.dbo.georeference_view_positions_gst_indicators as "localization" 

--		
--		
--where 
--		"position".Unidad in ('TT1321') 
--order by 
--		Unidad
--	and 
--		"position".Latitud between "localization".min_latitud and "localization".max_latitud
--	and 
--		"position".Longitud between "localization".min_longitud and "localization".max_longitud
	 
	 
	 
	 
--select max("trip".no_viaje),id_area,viajeactual from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = 'TT1321'
--group by 
----		no_viaje,
--		id_area,viajeactual
--		
		
--		
--select id_area,* from gstdb.dbo.mtto_unidades where id_unidad = 'TT1321'
--		
--		
--		
--select * from gstdb.dbo.mtto_unidades where id_unidad = 'TT1321'
--
--select * from gstdb.dbo.desp_posicion_unidad where id_unidad = 'TT1321'
--
--select no_viaje,* from gstdb.dbo.desp_asignacion where id_unidad = 'TT1321'
--		

select * from sistemas.dbo.georeference_view_positions_hist_gst_indicators where Area = base and statdesc in (0) and inBase = 0  
union all
select * from sistemas.dbo.georeference_view_positions_hist_gst_indicators where Area = base and statdesc = 1 


use sistemas

select 
		 "holidays".id
		,"holidays".cyear
		,"holidays".id_month
		,"holidays"."month"
		,"holidays".period
		,"holidays"._period
		,"holidays".first_day_in_month
		,"holidays".last_day_in_month
		,"holidays".is_current
		,"holidays".hasSun
		,"holidays".hasFullSun
		,"holidays".hasCurrentHoliday
		,"holidays".hisFullHoliday
		,"holidays".labDays
		,"holidays".labFullDays
		,"holidays".labRestDays
from sistemas.dbo.ingresos_costos_gst_holidays as "holidays"




--
-- where UNIDAD = 'TT1321' 
--order by UNIDAD
 

--select * from gstdb.dbo.desp_asignacion where no_viaje = 0		

use sistemas
   -- select * from sistemas.dbo.georeference_view_positions_documents_gst_indicators where Area = 'MEXICALI' order by unidad
   
IF OBJECT_ID ('georeference_view_positions_documents_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_documents_gst_indicators;
-- now build the view
create view georeference_view_positions_documents_gst_indicators
with encryption
as
select
	 "pos".id
	,"pos".AsignacionDesc
	,"pos".Seguimiento
	,"pos".StatusDescription
	,"pos".statdesc
	,"pos".enBase
	,"pos".inBase
	,"pos".DescriptionViaje
	,"pos".traceroute
	,"pos".id_area
	,"pos".Area
	,"pos".Unidad
	,"pos".Asignacion
	,"pos".status_asignacion
	,"pos".seguimiento_actual
	,"pos".FechaAsignacion
	,"pos".Viaje
	,"pos".Guias
	,"pos".Despachado
	,"pos".Operador
	,"pos".Latitud
	,"pos".Longitud
	,"pos".FechaPosition
--	,"pos".id_geocerca
	,"pos".base
	,"pos".max_latitud
	,"pos".min_latitud
	,"pos".max_longitud
	,"pos".min_longitud
--	,current_timestamp as 'created'
--	,1 as 'status'
--	,1 as 'control_id'
from 
	sistemas.dbo.georeference_view_positions_hist_gst_indicators as "pos"
where
	base = Area --and Latitud is null
	and 
		(
			statdesc = 0
		and 
			inBase = 0
		or
			statdesc = 1 
		)
		
		
		
--	select 
--			 "geor".Area
--			,count("geor".Unidad) as "unidades"
--		    ,"geor".StatusDescription
----		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int) as 'despacho'
--		    ,cast(substring( convert(nvarchar(MAX), "geor".created, 112) , 1, 6 ) as int) as 'periodo'
--	from 
----			sistemas.dbo.georeference_view_positions_documents_gst_indicators as "geor"
--			sistemas.dbo.georeference_tbl_positions_documents_gst_indicators as "geor"
--	group by 
--			 "geor".Area
--		    ,"geor".StatusDescription
----		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int)
--			,cast(substring( convert(nvarchar(MAX), "geor".created, 112) , 1, 6 ) as int)

		
-- ============================================================================================================ --
		
use sistemas
   -- select * from sistemas.dbo.georeference_view_positions_dash_main_gst_indicators order by unidad 
IF OBJECT_ID ('georeference_view_positions_dash_main_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_dash_main_gst_indicators;
-- now build the view
create view georeference_view_positions_dash_main_gst_indicators
with encryption
as
	select 
			 row_number() over(order by "geor".Area) as 'id'
			,"geor".Area
			,count("geor".Unidad) as "unidades"
		    ,"geor".StatusDescription
		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int) as 'despacho'
		    ,cast(substring( convert(nvarchar(MAX), current_timestamp , 112) , 1, 6 ) as int) as 'periodo'
	from 
			sistemas.dbo.georeference_view_positions_documents_gst_indicators as "geor"
--			sistemas.dbo.georeference_tbl_positions_documents_gst_indicators as "geor"
	group by 
			 "geor".Area
		    ,"geor".StatusDescription
		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int)
--			,cast(substring( convert(nvarchar(MAX), current_timestamp , 112) , 1, 6 ) as int)


-- =================================================================================================== --	
-- =============  Build of store table for Description and Compromise Date
-- =================================================================================================== --		

use sistemas
-- Ambagasdowa of the future check the Capacity for build and manual historical by id add foreign key for that
ALTER PROCEDURE georeference_store_control_positions
--    @control_id int,      
AS   
	declare @id_control int
    SET NOCOUNT ON;  
	insert into sistemas.dbo.georeference_tbl_positions_control_gst_indicators
   		select 
				 cast(current_timestamp as date) as 'inserted'
				,cast(substring( convert(nvarchar(MAX), CURRENT_TIMESTAMP, 112) , 1, 6 ) as int) as 'periodo'
				,day(CURRENT_TIMESTAMP) as 'day'
				,current_timestamp as 'created'	
				,1 as 'status'
	-- get the last id of us![Konami?}
   	select @id_control = @@IDENTITY
--   	select @id_control
 	-- XD
	insert into sistemas.dbo.georeference_tbl_positions_documents_gst_indicators	
	select
		 "pos".AsignacionDesc
		,"pos".Seguimiento
		,"pos".StatusDescription
		,"pos".statdesc
		,"pos".enBase
		,"pos".inBase
		,"pos".DescriptionViaje
		,"pos".traceroute
		,"pos".id_area
		,"pos".Area
		,"pos".Unidad
		,"pos".Asignacion
		,"pos".status_asignacion
		,"pos".seguimiento_actual
		,"pos".FechaAsignacion
		,"pos".Viaje
		,"pos".Guias
		,"pos".Despachado
		,"pos".Operador
		,"pos".Latitud
		,"pos".Longitud
		,"pos".FechaPosition
		,"pos".base
		,"pos".max_latitud
		,"pos".min_latitud
		,"pos".max_longitud
		,"pos".min_longitud
		,@id_control as 'control_id'
		,cast(current_timestamp as date) as 'inserted'
		,cast(substring( convert(nvarchar(MAX), CURRENT_TIMESTAMP, 112) , 1, 6 ) as int) as 'periodo'
		,day(CURRENT_TIMESTAMP) as 'day'
		,current_timestamp as 'created'
		,1 as 'status'
	from 
		sistemas.dbo.georeference_view_positions_hist_gst_indicators as "pos"
	where
		base = Area
	and 
		(
			statdesc = 0
		and 
			inBase = 0
		or
			statdesc = 1 
		)
		
 -- =================================================================================================== --	
		


use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.georeference_tbl_positions_documents_gst_indicators', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.georeference_tbl_positions_documents_gst_indicators 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table georeference_tbl_positions_documents_gst_indicators(
		 id								int 			identity(1,1)
		,AsignacionDesc			char(55) 		null
		,Seguimiento				char(55)		null 
		,StatusDescription		char(55)		null
		,statdesc					int				null
		,enBase					char(55)		null
		,inBase					int				null
		,DescriptionViaje			char(80)		null
		,traceroute				int				null
		,id_area				int		null
		,Area						char(55)		null
		,Unidad					char(55)		null
		,Asignacion				int				null
		,status_asignacion		int				null
		,seguimiento_actual		int				null
		,FechaAsignacion			datetime		null
		,Viaje					int				null
		,Guias					text			null
		,Despachado				datetime		null
		,Operador					char(120)		null
		,Latitud					decimal			null
		,Longitud					decimal			null
		,FechaPosition			datetime		null
--		,id_geocerca				bigint			null
		,base						char(55)		null
		,max_latitud				decimal			null
		,min_latitud				decimal			null
		,max_longitud				decimal			null
		,min_longitud				decimal			null
		,control_id				int				null
		,inserted			date			null
		,periodo			int				null
		,"day"				int				null
		,created			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off
-- go
-- test

--
--
----declare @tbl_awebo table
--select * from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo
--use [sistemas]
---- go
--IF OBJECT_ID('sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo', 'U') IS NOT NULL 
--  DROP TABLE sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo  
--set ansi_nulls on
---- go
--set quoted_identifier on
---- go
--set ansi_padding on
---- go
--create table georeference_tbl_positions_documents_gst_indicators_respaldo(
--		 id						int 
--		,AsignacionDesc			char(55) 		null
--		,Seguimiento				char(55)		null 
--		,StatusDescription		char(55)		null
--		,statdesc					int				null
--		,enBase					char(55)		null
--		,inBase					int				null
--		,DescriptionViaje			char(80)		null
--		,traceroute				int				null
--		,id_area				int		null
--		,Area						char(55)		null
--		,Unidad					char(55)		null
--		,Asignacion				int				null
--		,status_asignacion		int				null
--		,seguimiento_actual		int				null
--		,FechaAsignacion			datetime		null
--		,Viaje					int				null
----		,Guias					text			null
--		,Despachado				datetime		null
--		,Operador					char(120)		null
--		,Latitud					decimal			null
--		,Longitud					decimal			null
--		,FechaPosition			datetime		null
----		,id_geocerca				bigint			null
--		,base						char(55)		null
--		,max_latitud				decimal			null
--		,min_latitud				decimal			null
--		,max_longitud				decimal			null
--		,min_longitud				decimal			null
--		,control_id				int				null
--		,inserted			date			null
--		,periodo			int				null
--		,"day"				int				null
--		,created			datetime		null
--		,status				tinyint 		default 1 null
--);
--set ansi_padding off
--
--insert into sistemas.dbo.georeference_tbl_positions_documents_gst_indicators
--	select 
----		 "tbl".id
--		 "tbl".AsignacionDesc
--		,"tbl".Seguimiento
--		,"tbl".StatusDescription
--		,"tbl".statdesc
--		,"tbl".enBase
--		,"tbl".inBase
--		,"tbl".DescriptionViaje
--		,"tbl".traceroute
--		,"tbl".id_area
--		,"tbl".Area
--		,"tbl".Unidad
--		,"tbl".Asignacion
--		,"tbl".status_asignacion
--		,"tbl".seguimiento_actual
--		,"tbl".FechaAsignacion
--		,"tbl".Viaje
--		,'' as 'Guias'
--		,"tbl".Despachado
--		,"tbl".Operador
--		,"tbl".Latitud
--		,"tbl".Longitud
--		,"tbl".FechaPosition
--		,"tbl".base
--		,"tbl".max_latitud
--		,"tbl".min_latitud
--		,"tbl".max_longitud
--		,"tbl".min_longitud
--		,"tbl".control_id
--		,"tbl".inserted
--		,"tbl".periodo
--		,"tbl".day
--		,"tbl".created
--		,"tbl".status
--	from georeference_tbl_positions_documents_gst_indicators_respaldo as "tbl"
--	
--	



	
use [sistemas]
--use master
-- 
-- delete from sistemas.dbo.georeference_tbl_positions_control_gst_indicators where id in (3,4)
-- select * from georeference_tbl_positions_control_gst_indicators
-- go
IF OBJECT_ID('georeference_tbl_positions_control_gst_indicators', 'U') IS NOT NULL 
  DROP TABLE georeference_tbl_positions_control_gst_indicators 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
	create table georeference_tbl_positions_control_gst_indicators(
		 id					bigint 			identity(1,1)
		,inserted			date			null
		,periodo			int				null
		,"day"				int				null
		,created			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off
	


--CREATE TABLE sistemas.dbo.foo(ID INT IDENTITY(1,1), name SYSNAME);
--
----We can capture IDENTITY values in a table variable for further consumption.
--
--DECLARE @IDs TABLE(ID INT);
--
---- minor change to INSERT statement; add an OUTPUT clause:
--drop table sistemas.dbo.foo
--INSERT sistemas.dbo.foo(name) 
--  OUTPUT inserted.ID INTO @IDs(ID)
--SELECT N'Fred'
--UNION ALL
--SELECT N'Bob';




use sistemas
-- select * from sistemas.dbo.georeference_view_positions_gst_indicators   
IF OBJECT_ID ('georeference_view_positions_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_gst_indicators;
-- now build the view
create view georeference_view_positions_gst_indicators
with encryption
as	
with "pos_lat_max" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (146)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lat_min" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 146 as 'id_geocerca' --"dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (147)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lon_max" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
		union all 
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (146)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lon_min" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 146 as 'id_geocerca' -- "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
		where 
			"dg".id_geocerca in (147)
		group by 
			 "dg".id_geocerca
			,"dg".nombre
)
select 
--	 "pos_lat_max".id_geocerca
     row_number() over(order by "pos_lat_max".base) as 'id'
	,"pos_lat_max".base
	,"pos_lat_max".latitud as 'max_latitud'
	,"pos_lat_min".latitud as 'min_latitud'
	,"pos_lon_max".longitud as 'max_longitud'
	,"pos_lon_min".longitud as 'min_longitud'
from 
	"pos_lat_max"
inner join 
	"pos_lat_min"
on 
	"pos_lat_max".id_geocerca = "pos_lat_min".id_geocerca
inner join 
	"pos_lon_max"
on 
	"pos_lat_max".id_geocerca = "pos_lon_max".id_geocerca
inner join 
	"pos_lon_min"
on 
	"pos_lat_max".id_geocerca = "pos_lon_min".id_geocerca


	
-- ==================================================================================================== --
-- update sistemas.dbo.georeference_tbl_positions_control_gst_indicators set inserted = '2019-03-12' where id = 1
--  Schematics for Unidades sin Documentos
--  	exec georeference_store_control_positions
  	select * from sistemas.dbo.georeference_tbl_positions_control_gst_indicators
	select control_id,inserted from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators group by control_id,inserted
  
--	delete from sistemas.dbo.georeference_tbl_positions_control_gst_indicators where id in (2,5,6)
--	delete from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators where control_id in (2)
--
-- ==================================================================================================== --

	
--	select 
--			"viaje".id_area
--			,"area".ciudad as 'area'
--			,Count ("viaje".no_viaje) as 'total_viajes'
--			,"asignacion".id_seguimiento
--			,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'periodo'
--	from 
--			gstdb.dbo.trafico_viaje as "viaje"
--	inner join 
--			gstdb.dbo.desp_asignacion as "asignacion"
--		on 
--			"viaje".no_viaje = "asignacion".no_viaje and "viaje".id_area = "asignacion".id_area --and "asignacion".id_seguimiento = 1 and "asignacion".status_asignacion in (2,3)
--	inner join 
--			gstdb.dbo.general_area as "area" 
--		on 
--			"viaje".id_area = "area".id_area 
--	where
--			"viaje".id_area = 1
--	group by
--			"viaje".id_area
--			,"area".ciudad
--			,"asignacion".id_seguimiento
--			,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int)
--			
--	
--	
--	select sum(trip_count),area,periodo_despachado from sistemas.dbo.projections_view_full_gst_core_indicators
--	where id_area = 1 
--	group by area,periodo_despachado
	
--	
--	1- Cantidad de viaje mensuales  add desp_asignacion.id_seguimento 1=cargado , 2=vacio [Despachdos]
--
--	2- Monto mensual facturado tipo_doc = 1 , total = subtotal + iva - retencion
--	
--	3- Cantidad Mensual de fletes por poblacion
--	
--	4- Monto por unidad facturado
	
	
	select id_seguimiento,status_asignacion from sistemas.dbo.projections_view_full_gst_testcore_indicators
	
-- 32421 32421
-- 32420	

-- ==================================================================================================== --
--	Indicador Cantidad de viaje mensuales
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_viajes_mensual_gst_indicators   
IF OBJECT_ID ('resumen_view_viajes_mensual_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_viajes_mensual_gst_indicators;
-- now build the view
create view resumen_view_viajes_mensual_gst_indicators
with encryption
as	
	select 
			 row_number() over(order by "indicador".id_area) as 'id'
			,"indicador".id_area
			,"indicador".area as 'area'
			,sum("indicador".trip_count) as 'viajes'
			,"indicador".id_seguimiento
			,case 
				"indicador".id_seguimiento
					when 1 then 'ViajeCargado'
					when 2 then 'ViajeVacio'
			 end as 'TipoViaje'
			,"indicador".periodo_despachado as 'periodo'
	from 
			sistemas.dbo.projections_view_full_gst_core_indicators as "indicador"
	group by 
			"indicador".id_area
			,"indicador".area
			,"indicador".id_seguimiento
			,"indicador".periodo_despachado
		
			
-- ==================================================================================================== --
--	Indicador Cantidad Mensual de fletes por poblacion
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_viajes_mensualpoblacion_gst_indicators   
IF OBJECT_ID ('resumen_view_viajes_mensualpoblacion_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_viajes_mensualpoblacion_gst_indicators;
-- now build the view
create view resumen_view_viajes_mensualpoblacion_gst_indicators
with encryption
as
	select 
			 row_number() over(order by "indicador".id_area) as 'id'
			,"indicador".id_area
			,"indicador".area as 'area'
			,sum("indicador".trip_count) as 'viajes'
			,case 
				"indicador".id_seguimiento
					when 1 then 'ViajeCargado'
					when 2 then 'ViajeVacio'
			 end as 'TipoViaje'
			,"indicador".periodo_despachado as 'periodo'
			,"indicador".id_destino
			,"destino".desc_plaza as 'poblacion'
	from 
			sistemas.dbo.projections_view_full_gst_core_indicators as "indicador"
	inner join 
			gstdb.dbo.trafico_plaza as "destino" on "destino".id_plaza = "indicador".id_destino
	where 
			"indicador".id_seguimiento = 1	
--		and 
--			"indicador".periodo_despachado = 201902 and "indicador".id_area = 1
	group by 
			"indicador".id_area
			,"indicador".area
			,"indicador".id_seguimiento
			,"indicador".periodo_despachado
			,"indicador".id_destino
			,"destino".desc_plaza
			
			
-- ==================================================================================================== --
-- Monto mensual facturado
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_montofacturado_mensual_gst_indicators   
IF OBJECT_ID ('resumen_view_montofacturado_mensual_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_montofacturado_mensual_gst_indicators;
-- now build the view
create view resumen_view_montofacturado_mensual_gst_indicators
with encryption
as
	select 
			 row_number() over(order by "guia".id_area) as 'id'
			,"guia".id_area
			,"area".ciudad as 'area'
			,sum("guia".flete) as 'flete'
			,sum("guia".subtotal) as 'subtotal'
			,sum("guia".iva_guia) as 'iva'
			,sum("guia".monto_retencion) as 'retencion'
			,sum("guia".subtotal + "guia".iva_guia - "guia".monto_retencion) as 'total'
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'periodo'
	from 
			gstdb.dbo.trafico_guia as "guia"
	inner join 
			gstdb.dbo.general_area as "area"
		on 
			"area".id_area = "guia".id_area
	where 
			"guia".tipo_doc = 1
	group by 
			 "guia".id_area
			,"area".ciudad
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int)
	

	
-- ==================================================================================================== --
-- Monto mensual facturado por unidad
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_montofacturado_unidad_gst_indicators   
IF OBJECT_ID ('resumen_view_montofacturado_unidad_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_montofacturado_unidad_gst_indicators;
-- now build the view
create view resumen_view_montofacturado_unidad_gst_indicators
with encryption
as	
	select 
			 row_number() over(order by "guia".id_area) as 'id'
			,"guia".id_area
			,"area".ciudad as 'area'
			,sum("guia".flete) as 'flete'
			,sum("guia".subtotal) as 'subtotal'
			,sum("guia".iva_guia) as 'iva'
			,sum("guia".monto_retencion) as 'retencion'
			,sum("guia".subtotal + "guia".iva_guia - "guia".monto_retencion) as 'total'
			,"guia".id_unidad as 'unidad'
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'periodo'
	from 
			gstdb.dbo.trafico_guia as "guia"
	inner join 
			gstdb.dbo.general_area as "area"
		on 
			"area".id_area = "guia".id_area
	where 
			"guia".tipo_doc = 1
	group by 
			 "guia".id_area
			,"area".ciudad
			,"guia".id_unidad
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int)
	
	
	
	
	
	
	
	
