use sistemas
IF OBJECT_ID ('projections_view_full_dispatched_rpto_single_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_dispatched_rpto_single_periods;
create view projections_view_full_dispatched_rpto_single_periods
with encryption
as
select 
		 "desp".id_area
		,"desp".id_unidad
		,"desp".id_configuracionviaje
		,"desp".id_tipo_operacion
		,"desp".id_fraccion
		,"desp".id_flota
		,"desp".no_viaje
		,"desp".fecha_guia
		,"desp".mes
		,"desp".f_despachado
		,"desp".cliente
		,"desp".kms_viaje
		,"desp".kms_real
		,case -- which field we have to show
			when "desp".id_fraccion in ( 
									select "prfrt".projections_id_fraccion
									from sistemas.dbo.projections_view_company_fractions as "prfrt"
									where "prfrt".projections_corporations_id = "desp".company and "prfrt".projections_rp_fraction_id = 1) -- means granel
				then 
					("desp".kms_viaje*2)
			when "desp".id_fraccion not in (	
									select "prtrf".projections_id_fraccion
									from sistemas.dbo.projections_view_company_fractions as "prtrf"
									where "prtrf".projections_corporations_id = "desp".company and "prtrf".projections_rp_fraction_id = 1) -- means otros
				then 
					"desp".kms_real
			else
				"desp".kms_real
		end as 'kms'
		,"desp".subtotal
		,"desp".peso
		,"desp".configuracion_viaje
		,"desp".tipo_de_operacion
		,"desp".flota
		,"desp".area
		,"desp".fraccion
		,"desp".company
		,"desp".trip_count
		,year("desp".f_despachado) as 'year'
from 
		sistemas.dbo.projections_view_full_company_dispatched_indicators  as "desp"
where 
		"desp".area in ('MACUSPANA')
	and
		year("desp".f_despachado) in ( year( dateadd(year,-1,cast(CURRENT_TIMESTAMP as date))) , year(CURRENT_TIMESTAMP) )


